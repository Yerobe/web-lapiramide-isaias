<?php

namespace App\Http\Controllers;

use App\Models\Cliente;
use App\Models\Reserva;
use App\Models\Evento;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;


use App\Http\Requests\ClienteFormRequest;
use App\Http\Requests\ClientFormRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Date;

class ReservaController extends Controller
{

    // Este es la funcion para ir a la vista del pago y paso los datos que voy acarreando por la aplicacion
    public function goPay($Id, $fechaInicio, $fechaFinal, $adultos, $niños, $mascotas, $noches, $precio)
    {

        // Datos acarreados

        $arrayDatos = [
            "IdTarifa"    => $Id,
            "fechaInicio" => $fechaInicio,
            "fechaFinal"  => $fechaFinal,
            "adultos"     => $adultos,
            "niños"       => $niños,
            "mascotas"    => $mascotas,
            "noches"      => $noches,
            "precio"      => $precio
        ];


        // Array de paises que se pondran en el select
        $paises = DB::select('SELECT * FROM paises');



        return view('payReserv', ['arrayDatos' => $arrayDatos], ['arrayPaises' => $paises]);
    }


    // Funcion para crear la reserva y los clientes (Junto con el ClietFormRequest para validar el formulario)
    public function createReserva(ClientFormRequest $request, $Id, $fechaInicio, $fechaFinal, $adultos, $niños, $mascotas, $noches, $precio)
    {

        // Validacion y datos recogidos por post
        $validated = $request->validated();

        $nombre = $request->input('nombre');
        $apellidos = $request->input('apellidos');
        $NIden = $request->input("N#Identificativo");
        $email = $request->input('email');
        $prefTelefono = $request->input('prefTelefono');
        $telefono = $prefTelefono . $request->input('telefono');
        $pais = $request->input('pais');

        // Tarifa que se selecciono
        $tarifa = DB::table('tarifas')
            ->where('Id', $Id)
            ->get();

        // Array con los datos del cliente
        $arrayDatosCliente = [
            "Nombre"         => $nombre,
            "Apellidos"      => $apellidos,
            "NumeroIdentificativo"   => $NIden,
            "Email"          => $email,
            "Telefono"       => $telefono,
            "IdPais"           => $pais,
            "IdComunidadAutonoma"    => null
        ];




        // Condicional por si existe el usuario


        $NifCliente = DB::table("clientes")->where("NumeroIdentificativo", $NIden)->get();

        if (count($NifCliente) == 0) {
            // Creacion de el cliente si no existe su numero identificativo en la base de datos
            $IdCliente = DB::table('clientes')->insertGetId($arrayDatosCliente);

        } else {

            // Obtencion de el id del cliente si existe el numero identificativo en la base de datos
            $ClienteExistente = DB::table("clientes")->where("NumeroIdentificativo", $NIden)->get();

            $IdCliente = $ClienteExistente[0]->Id;
        }


        // Condicional por si existe el usuario




        // Available alpha caracters
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

        // generate a pin based on 2 * 7 digits + a random character
        $pin = mt_Rand(1000000, 9999999)
            . mt_Rand(1000000, 9999999)
            . $characters[Rand(0, strlen($characters) - 1)];

        // shuffle the result
        $string = str_shuffle($pin);




        // Condicional para los regimenes

        foreach ($tarifa as $tar) {

            $TipoRegimen = $tar->TipoRegimen;
        }


        if ($TipoRegimen == 1) {
            $Regimen = "Only Bed";
        } else if ($TipoRegimen == 2) {
            $Regimen = "Breakfast";
        } else if ($TipoRegimen == 3) {
            $Regimen = "Half Board";
        } else if ($TipoRegimen == 4) {
            $Regimen = "Full Board";
        }


        $CurrentDay = Date("Y-m-d");


        // Condicional para los regimenes


        // Datos de la reserva para su creacion
        $arrayDatosReserva = [ 
            "NumeroReserva"     => $string,
            "FechaCreacion"     => $CurrentDay,
            "BookDay"           => $CurrentDay,
            "FechaEntrada"      => $fechaInicio,
            "FechaSalida"       => $fechaFinal,
            "NumeroAdultos"     => $adultos,
            "NumeroNinios"      => $niños,
            "NumeroMascotas"    => $mascotas,
            "NombreCliente"     => $nombre,
            "ApellidosCliente"  => $apellidos,
            "Prepago"           => 0,
            "Precio_Total"      => $precio,
            "Comentario"        => null,
            "Estado"            => true,
            "Puntos"            => 0,
            "Regimen"           => $Regimen,
            "IdOperador"        => 5,
            "IdHabitacion"      => null,
            "IdEmpleadoCreado"  => null,
            "IdHotel"           => 1,
            "IdCliente"         => $IdCliente
        ];


        // Creacion de la reserva y obtencion del Id
        $IdReserva = DB::table('reservas')->insertGetId($arrayDatosReserva);

        // Obtencion de los 3 eventos finales de la base de datos
        $eventos = Evento::all();

        $arrayEventosCont = [];

        for ($i = 0; $i < 3; $i++) {

            $indice = count($eventos) - 1 - $i;

            array_push($arrayEventosCont, $eventos[$indice]);
        }


        // Array de dtos que podria mostrar en la vista
        $arrayDatosMostrar = [
            "nombre" => $nombre,
            "apellidos" => $apellidos,
            "fechaInicio" => $fechaInicio,
            "fechaFinal"  => $fechaFinal,
            "codigo" => $string,
            "IdReserva" => $IdReserva,
            "IdCliente" => $IdCliente,
            "noches" => $noches,
            "precio" => $precio
        ];

        
        // Recogiendo datos del clientte y de la reserva para mandar el email al cliente
        $arrayCliente = DB::table('clientes')->where('NumeroIdentificativo', $NIden)->get();

        if($IdCliente != null){
            foreach ($arrayCliente as $cliente) {

                $datosCliente = array('name' => $cliente->Nombre, 
                                      'surname'=>$arrayDatosMostrar["apellidos"], 
                                      'code' => $arrayDatosMostrar["codigo"], 
                                      'night' => $arrayDatosMostrar["noches"], 
                                      'price' => $arrayDatosMostrar["precio"],
                                      'checkin' => $arrayDatosMostrar["fechaInicio"],
                                      'checkout' => $arrayDatosMostrar["fechaFinal"],
                                      'adults' => $adultos,
                                      'children' => $niños,
                                      'pets' => $mascotas
                                    );
    
    
                
    
                Mail::send('mail', $datosCliente, function ($message) use ($cliente) {
    
                    $message->to($cliente->Email, $cliente->Nombre)->subject(env('APP_NAME'));
    
                    $message->from(env('MAIL_FROM_ADDRESS'), env('MAIL_USERNAME'));
    
                });
            }
        }

        
        
        // Creacion de las sessiones para el mostrado de la confirmacion del email y de la creacion de reserva
        $request->session()->flash('Mandado');

        $request->session()->flash('Realizada');

        return view('index', ['arrayEventos' => $arrayEventosCont], ['arrayDatosMostrar' => $arrayDatosMostrar]);
    }
}
