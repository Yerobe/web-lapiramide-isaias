<?php
  
namespace App\Http\Controllers;
  
use Illuminate\Http\Request;
use PDF;

use Illuminate\Support\Facades\DB;

use App\Models\Cliente;
use App\Models\Reserva;
  
class PDFController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    // Aqui pasamos los datos necesarios de la reserva que se imprimiran en el pdf
    public function generatePDF($IdReserva, $IdCliente, $noches, $precio)
    {

        // Busqueda de la reserva echa y el cliente creado

        $Reserva = DB::table('reservas')
        ->where('Id', $IdReserva)
        ->get();

        $Cliente = DB::table('clientes')
        ->where('Id', $IdCliente)
        ->get();
        
        

        // Recoleccion de los datos que se imprimiran
        $datos = [
            "Nombre" => $Cliente[0]->Nombre,
            "Apellidos" => $Cliente[0]->Apellidos,
            "NumeroIdentificativo" => $Cliente[0]->NumeroIdentificativo,
            "NumeroReserva" => $Reserva[0]->NumeroReserva,
            "FechaEntrada"  => $Reserva[0]->FechaEntrada,
            "FechaSalida"   => $Reserva[0]->FechaSalida,
            "NumeroAdultos" => $Reserva[0]->NumeroAdultos,
            "NumeroNinios"  => $Reserva[0]->NumeroNinios,
            "NumeroMascotas"=> $Reserva[0]->NumeroMascotas,
            "Noches"        => $noches,
            "Precio"        => $precio

        ];

        // Creacion del pdf
        $pdf = \PDF::loadView('myPDF', $datos);
    
        // Descarga del pdf
        return $pdf->download('reservalapiramide.pdf');
    }
}