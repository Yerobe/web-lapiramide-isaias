<?php

namespace App\Http\Controllers;

use App\Models\Evento;
use Illuminate\Http\Request;
use LengthException;

class HomeController extends Controller
{
 
    // Funcion en la que vamos a la vista principal con los 3 ultimos eventos de la base de datos
    public function getHome(){


        $eventos = Evento::all();

        $arrayEventosCont = [];

        for($i=0;$i<3;$i++){

           $indice = count($eventos)-1-$i;

           array_push($arrayEventosCont, $eventos[$indice]);
        
        }

        return view('index', ['arrayEventos' => $arrayEventosCont]);

    }



}
