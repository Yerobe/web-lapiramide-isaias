<?php

namespace App\Http\Controllers;

use App\Models\Tarifa;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use SebastianBergmann\CodeCoverage\Driver\Selector;

class RoomController extends Controller
{
    public function getRooms(Request $request)
    {

        // Datos recogidos en el formulario inicial

        $fechaInicio = $request->input('fecha_inicio');
        $fechaFinal = $request->input('fecha_final');
        $adultos = $request->input('adultos');
        $niños = $request->input('niños');
        $mascotas = $request->input('mascotas');

        // Calculo de las noches dependiendo de las fechas
        $noches = DB::select('SELECT TIMESTAMPDIFF(DAY, "' . $fechaInicio . '" , "' . $fechaFinal . '") as  noches');

        $noches = $noches[0]->noches;


        // Array de datos para las rutas 
        $arrayRuta = [
            "fechaInicio" => $fechaInicio,
            "fechaFinal"  => $fechaFinal,
            "adultos"     => $adultos,
            "niños"       => $niños,
            "mascotas"    => $mascotas,
            "noches"      => $noches,

        ];

        // Arrays de tarifas de cada regimen diferente


        // $tarifas = DB::select('SELECT * FROM tarifas WHERE NumeroAdultos = ' . $adultos . ' and NumeroNinos = ' . $niños .
        //     ' and ( "' . $fechaInicio . '" Between FechaInicio and FechaFin  or  "' . $fechaFinal . '" Between FechaInicio and FechaFin)
        // GROUP BY Nombre');

        $tarifasSoloAlojamiento = DB::select('SELECT * FROM tarifas WHERE NumeroAdultos = ' . $adultos . ' and NumeroNinos = ' . $niños .
            ' and ( "' . $fechaInicio . '" Between FechaInicio and FechaFin  or  "' . $fechaFinal . '" Between FechaInicio and FechaFin) and Nombre LIKE "%Solo Alojamiento"');

        $tarifasDesayuno = DB::select('SELECT * FROM tarifas WHERE NumeroAdultos = ' . $adultos . ' and NumeroNinos = ' . $niños .
            ' and ( "' . $fechaInicio . '" Between FechaInicio and FechaFin  or  "' . $fechaFinal . '" Between FechaInicio and FechaFin) and Nombre LIKE "%Con Desayuno"');


        $tarifasMediaPension = DB::select('SELECT * FROM tarifas WHERE NumeroAdultos = ' . $adultos . ' and NumeroNinos = ' . $niños .
            ' and ( "' . $fechaInicio . '" Between FechaInicio and FechaFin  or  "' . $fechaFinal . '" Between FechaInicio and FechaFin) and Nombre LIKE "%Media Pension"');






        // Y apartir de aqui tengo la logica para calcular el precio dependiendo si se pillan dos temporadas o solo una 

        // SOLO ALOJAMIENTO



        if (count($tarifasSoloAlojamiento) == 2) {

            // DOS TARIFAS 


            for ($i = 0; $i <= count($tarifasSoloAlojamiento) - 2; $i++) {

                $diasPrimeraTarifa = DB::select('SELECT TIMESTAMPDIFF(DAY,"' . $fechaInicio . '" , "' . $tarifasSoloAlojamiento[$i]->FechaFin . '") as  noches');

                $diasSegundaTarifa = DB::select('SELECT TIMESTAMPDIFF(DAY,"' . $tarifasSoloAlojamiento[$i]->FechaFin . '" , "' . $fechaFinal . '") as  noches');

                $diasPri = $diasPrimeraTarifa[0]->noches;

                $diasSeg = $diasSegundaTarifa[0]->noches;

                $precioNochePrimeraTarifa = DB::select('SELECT PrecioNoche FROM tarifas WHERE Id = ' . $tarifasSoloAlojamiento[$i]->Id);

                $precioNochePrimeraTarifa = $precioNochePrimeraTarifa[$i]->PrecioNoche;

                $precioPrimeraTarifa = DB::select('SELECT SUM((' . $diasPri . ') * (' . $precioNochePrimeraTarifa . ')) as precio;');

                $precioPrimeraTarifa = $precioPrimeraTarifa[$i]->precio;

                $precioNocheSegundaTarifa = DB::select('SELECT PrecioNoche FROM tarifas WHERE Id = ' . $tarifasSoloAlojamiento[1]->Id);

                $precioNocheSegundaTarifa = $precioNocheSegundaTarifa[$i]->PrecioNoche;

                $precioSegundaTarifa = DB::select('SELECT SUM((' . $diasSeg . ') * (' . $precioNocheSegundaTarifa . ')) as precio;');

                $precioSegundaTarifa = $precioSegundaTarifa[$i]->precio;
            }

            $precioTotalSoloAlojamientos = $precioPrimeraTarifa + $precioSegundaTarifa;

            $precioTotalSoloAlojamientos = "$precioTotalSoloAlojamientos";

            $arraySoloAlojamiento = [
                "Id" => $tarifasSoloAlojamiento[1]->Id,
                "Nombre" => $tarifasSoloAlojamiento[1]->Nombre,
                "fechaInicio" => $fechaInicio,
                "fechaFinal"  => $fechaFinal,
                "noches"      => $noches,
                "adultos"     => $adultos,
                "niños"       => $niños,
                "mascotas"    => $mascotas,
                "PrecioTotal" => $precioTotalSoloAlojamientos
            ];

            // DOS TARIFAS 

        } else if (count($tarifasSoloAlojamiento) == 1) {
            // UNA TARIFA

            $diasTarifa = DB::select('SELECT TIMESTAMPDIFF(DAY, "' . $fechaInicio . '" , "' . $fechaFinal . '") as  noches');

            $diasTarifa = $diasTarifa[0]->noches;

            $precioNocheTarifa = DB::select('SELECT PrecioNoche FROM tarifas WHERE Id = ' . $tarifasSoloAlojamiento[0]->Id);

            $precioNocheTarifa = $precioNocheTarifa[0]->PrecioNoche;

            $precioTarifa = DB::select('SELECT SUM((' . $diasTarifa . ') * (' . $precioNocheTarifa . ')) as precio;');

            $precioTarifa = $precioTarifa[0]->precio;

            $arraySoloAlojamiento = [
                "Id" => $tarifasSoloAlojamiento[0]->Id,
                "Nombre" => $tarifasSoloAlojamiento[0]->Nombre,
                "fechaInicio" => $fechaInicio,
                "fechaFinal"  => $fechaFinal,
                "noches"      => $noches,
                "adultos"     => $adultos,
                "niños"       => $niños,
                "mascotas"    => $mascotas,
                "PrecioTotal" => $precioTarifa
            ];

            // UNA TARIFA
        }


        // SOLO ALOJAMIENTO

        /////////////////////////////////////////////////////

        // DESAYUNO



        if (count($tarifasDesayuno) == 2) {

            // DOS TARIFAS 


            for ($i = 0; $i <= count($tarifasDesayuno) - 2; $i++) {

                $diasPrimeraTarifa = DB::select('SELECT TIMESTAMPDIFF(DAY,"' . $fechaInicio . '" , "' . $tarifasDesayuno[$i]->FechaFin . '") as  noches');

                $diasSegundaTarifa = DB::select('SELECT TIMESTAMPDIFF(DAY,"' . $tarifasDesayuno[$i]->FechaFin . '" , "' . $fechaFinal . '") as  noches');

                $diasPri = $diasPrimeraTarifa[0]->noches;

                $diasSeg = $diasSegundaTarifa[0]->noches;

                $precioNochePrimeraTarifa = DB::select('SELECT PrecioNoche FROM tarifas WHERE Id = ' . $tarifasDesayuno[$i]->Id);

                $precioNochePrimeraTarifa = $precioNochePrimeraTarifa[$i]->PrecioNoche;

                $precioPrimeraTarifa = DB::select('SELECT SUM((' . $diasPri . ') * (' . $precioNochePrimeraTarifa . ')) as precio;');

                $precioPrimeraTarifa = $precioPrimeraTarifa[$i]->precio;

                $precioNocheSegundaTarifa = DB::select('SELECT PrecioNoche FROM tarifas WHERE Id = ' . $tarifasDesayuno[1]->Id);

                $precioNocheSegundaTarifa = $precioNocheSegundaTarifa[$i]->PrecioNoche;

                $precioSegundaTarifa = DB::select('SELECT SUM((' . $diasSeg . ') * (' . $precioNocheSegundaTarifa . ')) as precio;');

                $precioSegundaTarifa = $precioSegundaTarifa[$i]->precio;
            }

            $precioTotalDesayunos = $precioPrimeraTarifa + $precioSegundaTarifa;

            $precioTotalDesayunos = "$precioTotalDesayunos";

            $arrayDesayunos = [
                "Id" => $tarifasDesayuno[1]->Id,
                "Nombre" => $tarifasDesayuno[1]->Nombre,
                "fechaInicio" => $fechaInicio,
                "fechaFinal"  => $fechaFinal,
                "noches"      => $noches,
                "adultos"     => $adultos,
                "niños"       => $niños,
                "mascotas"    => $mascotas,
                "PrecioTotal" => $precioTotalDesayunos
            ];

            // DOS TARIFAS 

        } else if (count($tarifasDesayuno) == 1) {
            // UNA TARIFA

            $diasTarifa = DB::select('SELECT TIMESTAMPDIFF(DAY, "' . $fechaInicio . '" , "' . $fechaFinal . '") as  noches');

            $diasTarifa = $diasTarifa[0]->noches;

            $precioNocheTarifa = DB::select('SELECT PrecioNoche FROM tarifas WHERE Id = ' . $tarifasDesayuno[0]->Id);

            $precioNocheTarifa = $precioNocheTarifa[0]->PrecioNoche;

            $precioTarifa = DB::select('SELECT SUM((' . $diasTarifa . ') * (' . $precioNocheTarifa . ')) as precio;');

            $precioTarifa = $precioTarifa[0]->precio;

            $arrayDesayunos = [
                "Id" => $tarifasDesayuno[0]->Id,
                "Nombre" => $tarifasDesayuno[0]->Nombre,
                "fechaInicio" => $fechaInicio,
                "fechaFinal"  => $fechaFinal,
                "noches"      => $noches,
                "adultos"     => $adultos,
                "niños"       => $niños,
                "mascotas"    => $mascotas,
                "PrecioTotal" => $precioTarifa
            ];

            // UNA TARIFA
        }


        // DESAYUNO


        ////////////////////////////////////////////////////////////////////////////



        // MEDIA PENSION



        if (count($tarifasMediaPension) == 2) {

            // DOS TARIFAS 


            for ($i = 0; $i <= count($tarifasMediaPension) - 2; $i++) {

                $diasPrimeraTarifa = DB::select('SELECT TIMESTAMPDIFF(DAY,"' . $fechaInicio . '" , "' . $tarifasMediaPension[$i]->FechaFin . '") as  noches');

                $diasSegundaTarifa = DB::select('SELECT TIMESTAMPDIFF(DAY,"' . $tarifasMediaPension[$i]->FechaFin . '" , "' . $fechaFinal . '") as  noches');

                $diasPri = $diasPrimeraTarifa[0]->noches;

                $diasSeg = $diasSegundaTarifa[0]->noches;

                $precioNochePrimeraTarifa = DB::select('SELECT PrecioNoche FROM tarifas WHERE Id = ' . $tarifasMediaPension[$i]->Id);

                $precioNochePrimeraTarifa = $precioNochePrimeraTarifa[$i]->PrecioNoche;

                $precioPrimeraTarifa = DB::select('SELECT SUM((' . $diasPri . ') * (' . $precioNochePrimeraTarifa . ')) as precio;');

                $precioPrimeraTarifa = $precioPrimeraTarifa[$i]->precio;

                $precioNocheSegundaTarifa = DB::select('SELECT PrecioNoche FROM tarifas WHERE Id = ' . $tarifasMediaPension[1]->Id);

                $precioNocheSegundaTarifa = $precioNocheSegundaTarifa[$i]->PrecioNoche;

                $precioSegundaTarifa = DB::select('SELECT SUM((' . $diasSeg . ') * (' . $precioNocheSegundaTarifa . ')) as precio;');

                $precioSegundaTarifa = $precioSegundaTarifa[$i]->precio;
            }

            $precioTotalMediaPension = $precioPrimeraTarifa + $precioSegundaTarifa;

            $precioTotalMediaPension = "$precioTotalMediaPension";

            
            $arrayMediaPension = [
                "Id" => $tarifasMediaPension[1]->Id,
                "Nombre" => $tarifasMediaPension[1]->Nombre,
                "fechaInicio" => $fechaInicio,
                "fechaFinal"  => $fechaFinal,
                "noches"      => $noches,
                "adultos"     => $adultos,
                "niños"       => $niños,
                "mascotas"    => $mascotas,
                "PrecioTotal" => $precioTotalMediaPension
            ];

            // DOS TARIFAS 

        } else if (count($tarifasMediaPension) == 1) {
            // UNA TARIFA

            $diasTarifa = DB::select('SELECT TIMESTAMPDIFF(DAY, "' . $fechaInicio . '" , "' . $fechaFinal . '") as  noches');

            $diasTarifa = $diasTarifa[0]->noches;

            $precioNocheTarifa = DB::select('SELECT PrecioNoche FROM tarifas WHERE Id = ' . $tarifasMediaPension[0]->Id);

            $precioNocheTarifa = $precioNocheTarifa[0]->PrecioNoche;

            $precioTarifa = DB::select('SELECT SUM((' . $diasTarifa . ') * (' . $precioNocheTarifa . ')) as precio;');

            $precioTarifa = $precioTarifa[0]->precio;

            $arrayMediaPension = [
                "Id" => $tarifasMediaPension[0]->Id,
                "Nombre" => $tarifasMediaPension[0]->Nombre,
                "fechaInicio" => $fechaInicio,
                "fechaFinal"  => $fechaFinal,
                "noches"      => $noches,
                "adultos"     => $adultos,
                "niños"       => $niños,
                "mascotas"    => $mascotas,
                "PrecioTotal" => $precioTarifa
            ];

            // UNA TARIFA
        }


        // MEDIA PENSION


        // return view('rooms', ['arrayRuta' => $arrayRuta], ['arraySoloAlojamiento' => $arraySoloAlojamiento], ['arrayDesayunos' => $arrayDesayunos], ['arrayMediaPension' => $arrayMediaPension]);
            return view('rooms')->with('arrayRuta', $arrayRuta)->with('arraySoloAlojamiento', $arraySoloAlojamiento)->with('arrayDesayunos', $arrayDesayunos)->with('arrayMediaPension', $arrayMediaPension);
    }




    public function createRooms(Request $request, $Id, $fechaInicio, $fechaFinal, $adultos, $niños, $mascotas, $noches, $precio)
    {



        //Busqueda de la tarifa que tengo que relacionar con el cliente

        $tarifa = DB::table('tarifas')
            ->where('Id', $Id)
            ->get();

        //Busqueda de la tarifa que tengo que relacionar con el cliente


        //Datos que pasa en el primer formulario
        $arrayDatos = [
            "IdTarifa"    => $Id,
            "fechaInicio" => $fechaInicio,
            "fechaFinal"  => $fechaFinal,
            "adultos"     => $adultos,
            "niños"       => $niños,
            "mascotas"    => $mascotas,
            "noches"      => $noches,
            "precio"      => $precio
        ];
        //Datos que pasa en el primer formulario

        return view('selectRoom', ['arrayTarifa' => $tarifa], ['arrayDatos' => $arrayDatos]);
    }
}
