<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClientFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // Validacion del formulario de cliente
        return [
            'nombre' => 'required|max:100',
            'apellidos' => 'required|max:100',
            'email' => 'required|email',
            'N#Identificativo' => 'required|regex:/^([a-zA-Z0-9]){9,16}$/',
            'prefTelefono' => 'required|regex:/^\+[\d]{2,3}$/',
            'telefono' => 'required|regex:/^[\d]{9}$/',
            'pais' => 'required'

        ];
    }
}
