<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\ReservaController;
use App\Http\Controllers\RoomController;
use App\Http\Controllers\PDFController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



// Ruta Index (Ruta principal)

Route::get('/', [HomeController::class, 'getHome']);

// Ruta Index (Ruta principal)

// Ruta Tarifas (Rooms)

Route::post('/rooms', [RoomController::class, 'getRooms']);

// Ruta Tarifas (Rooms)

// Ruta resumen tarifa (selectedRoom)

Route::get('/room/create/{Id}/{fechaInicio}/{fechaFinal}/{adultos}/{ninos}/{mascotas}/{noches}/{precio}', [RoomController::class, 'createRooms']);

// Ruta resumen tarifa (selectedRoom)

// Ruta Pasarela de Pago (payReserv)

Route::get('/payment/{Id}/{fechaInicio}/{fechaFinal}/{adultos}/{ninos}/{mascotas}/{noches}/{precio}', [ReservaController::class, 'goPay']);

// Ruta Pasarela de Pago (payReserv)

// Ruta Creacion de reservas y clientes + mandado de emails

Route::post('/payment/createReserv/{Id}/{fechaInicio}/{fechaFinal}/{adultos}/{ninos}/{mascotas}/{noches}/{precio}', [ReservaController::class, 'createReserva']);

// Ruta Creacion de reservas y clientes + mandado de emails

// Ruta para generar los PDF

Route::get('/generate-pdf/{IdReserva}/{IdCliente}/{noches}/{precio}', [PDFController::class, 'generatePDF']);

// Ruta para generar los PDF