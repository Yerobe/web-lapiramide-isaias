<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToEmpleadosHasReservasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('empleados_has_reservas', function (Blueprint $table) {
            $table->foreign('IdEmpleado', 'FK13')->references('Id')->on('empleados')->onUpdate('NO ACTION')->onDelete('RESTRICT');
            $table->foreign('IdReserva', 'FK14')->references('Id')->on('reservas')->onUpdate('NO ACTION')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('empleados_has_reservas', function (Blueprint $table) {
            $table->dropForeign('FK13');
            $table->dropForeign('FK14');
        });
    }
}
