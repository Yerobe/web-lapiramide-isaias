<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToReservasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reservas', function (Blueprint $table) {
            $table->foreign('IdOperador', 'FK5')->references('Id')->on('operadores')->onUpdate('NO ACTION')->onDelete('RESTRICT');
            $table->foreign('IdHabitacion', 'FK6')->references('Id')->on('habitaciones')->onUpdate('NO ACTION')->onDelete('RESTRICT');
            $table->foreign('IdEmpleadoCreado', 'FK7')->references('Id')->on('empleados')->onUpdate('NO ACTION')->onDelete('RESTRICT');
            $table->foreign('IdHotel', 'FK8')->references('Id')->on('hoteles')->onUpdate('NO ACTION')->onDelete('RESTRICT');
            $table->foreign('IdCliente', 'FK9')->references('Id')->on('clientes')->onUpdate('NO ACTION')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reservas', function (Blueprint $table) {
            $table->dropForeign('FK5');
            $table->dropForeign('FK6');
            $table->dropForeign('FK7');
            $table->dropForeign('FK8');
            $table->dropForeign('FK9');
        });
    }
}
