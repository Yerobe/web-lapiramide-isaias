<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMensajesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mensajes', function (Blueprint $table) {
            $table->integer('Id')->primary();
            $table->string('Nombre', 125);
            $table->string('Email');
            $table->string('Asunto');
            $table->enum('Motivo', ['Precios', 'Sugerencias', 'Novedades', 'Eventos', 'Informar de un Problema']);
            $table->longText('Descripcion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mensajes');
    }
}
