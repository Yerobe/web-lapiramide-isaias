<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToFacturasReservasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('facturas_reservas', function (Blueprint $table) {
            $table->foreign('IdReserva', 'FK10')->references('Id')->on('reservas')->onUpdate('NO ACTION')->onDelete('RESTRICT');
            $table->foreign('IdEmpleado', 'FK11')->references('Id')->on('empleados')->onUpdate('NO ACTION')->onDelete('RESTRICT');
            $table->foreign('IdHotel', 'FK12')->references('Id')->on('hoteles')->onUpdate('NO ACTION')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('facturas_reservas', function (Blueprint $table) {
            $table->dropForeign('FK10');
            $table->dropForeign('FK11');
            $table->dropForeign('FK12');
        });
    }
}
