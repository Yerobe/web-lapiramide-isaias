<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedidos', function (Blueprint $table) {
            $table->integer('Id', true);
            $table->date('FechaRealizacion')->nullable();
            $table->date('FechaEntrega')->nullable();
            $table->time('HoraEntrega')->nullable();
            $table->float('PuntosConsumidos', 10, 0)->nullable();
            $table->enum('Entrega', ['Recoger', 'Entregar'])->nullable();
            $table->enum('Estado', ['No Realizado', 'Realizado', 'Completado', 'Cancelado'])->nullable();
            $table->integer('idUsuariodeAcceso')->nullable()->index('FK20');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedidos');
    }
}
