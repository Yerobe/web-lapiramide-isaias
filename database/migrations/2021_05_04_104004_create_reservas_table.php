<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReservasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservas', function (Blueprint $table) {
            $table->integer('Id', true);
            $table->string('NumeroReserva', 25);
            $table->date('FechaCreacion');
            $table->date('BookDay');
            $table->date('FechaEntrada');
            $table->date('FechaSalida');
            $table->smallInteger('NumeroAdultos');
            $table->smallInteger('NumeroNinios');
            $table->smallInteger('NumeroMascotas');
            $table->string('NombreCliente', 25);
            $table->string('ApellidosCliente', 50);
            $table->float('Prepago', 10, 0);
            $table->float('Precio_Total', 10, 0);
            $table->longText('Comentario')->nullable();
            $table->boolean('Estado');
            $table->float('Puntos', 10, 0);
            $table->enum('Regimen', ['Only Bed', 'Breakfast', 'Half Board', 'Full Board']);
            $table->integer('IdOperador')->index('FK5');
            $table->integer('IdHabitacion')->nullable()->index('FK6');
            $table->integer('IdEmpleadoCreado')->nullable()->index('FK7');
            $table->integer('IdHotel')->index('FK8');
            $table->integer('IdCliente')->nullable()->index('FK9');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservas');
    }
}
