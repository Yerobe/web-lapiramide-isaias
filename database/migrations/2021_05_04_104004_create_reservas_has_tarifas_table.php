<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReservasHasTarifasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservas_has_tarifas', function (Blueprint $table) {
            $table->integer('IdReserva');
            $table->integer('IdTarifa')->index('FK26');
            $table->date('Fecha')->nullable();
            $table->primary(['IdReserva', 'IdTarifa']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservas_has_tarifas');
    }
}
