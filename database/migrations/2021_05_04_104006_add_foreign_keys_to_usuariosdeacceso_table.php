<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToUsuariosdeaccesoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('usuariosdeacceso', function (Blueprint $table) {
            $table->foreign('idCliente', 'FK19')->references('Id')->on('clientes')->onUpdate('NO ACTION')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('usuariosdeacceso', function (Blueprint $table) {
            $table->dropForeign('FK19');
        });
    }
}
