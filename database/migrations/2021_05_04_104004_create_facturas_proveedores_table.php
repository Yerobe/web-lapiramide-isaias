<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFacturasProveedoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facturas_proveedores', function (Blueprint $table) {
            $table->integer('Id', true);
            $table->string('NumeroFactura', 125);
            $table->float('Precio', 10, 0);
            $table->date('FechaRecogida');
            $table->date('FechaRegistro');
            $table->date('FechaPago')->nullable();
            $table->boolean('Estado');
            $table->enum('MetodoPago', ['Efectivo', 'Transferencia', 'Tarjeta'])->nullable();
            $table->integer('IdProveedor')->index('FK16');
            $table->integer('IdEmpleado')->index('FK17');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facturas_proveedores');
    }
}
