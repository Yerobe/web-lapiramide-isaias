<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clientes', function (Blueprint $table) {
            $table->foreign('IdPais', 'FK3')->references('Id')->on('paises')->onUpdate('NO ACTION')->onDelete('RESTRICT');
            $table->foreign('IdComunidadAutonoma', 'FK4')->references('Id')->on('comunidadesautonomas')->onUpdate('NO ACTION')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clientes', function (Blueprint $table) {
            $table->dropForeign('FK3');
            $table->dropForeign('FK4');
        });
    }
}
