<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFacturasReservasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facturas_reservas', function (Blueprint $table) {
            $table->integer('Id', true);
            $table->char('NumeroFactura', 4)->unique('NumeroFactura');
            $table->string('Nombre', 125);
            $table->string('NumeroIdentificativo', 125);
            $table->string('Direccion')->nullable();
            $table->string('CodigoPostal', 15)->nullable();
            $table->string('Ciudad', 125)->nullable();
            $table->integer('IdReserva')->nullable()->index('FK10');
            $table->integer('IdEmpleado')->nullable()->index('FK11');
            $table->integer('IdHotel')->nullable()->index('FK12');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facturas_reservas');
    }
}
