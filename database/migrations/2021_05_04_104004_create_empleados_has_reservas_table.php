<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmpleadosHasReservasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empleados_has_reservas', function (Blueprint $table) {
            $table->integer('IdEmpleado');
            $table->integer('IdReserva')->index('FK14');
            $table->date('FechaModificacion')->nullable();
            $table->primary(['IdEmpleado', 'IdReserva']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empleados_has_reservas');
    }
}
