<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToClientesespecialesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clientesespeciales', function (Blueprint $table) {
            $table->foreign('IdCliente', 'FK23')->references('Id')->on('clientes')->onUpdate('NO ACTION')->onDelete('RESTRICT');
            $table->foreign('IdEmpleado', 'FK24')->references('Id')->on('empleados')->onUpdate('NO ACTION')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clientesespeciales', function (Blueprint $table) {
            $table->dropForeign('FK23');
            $table->dropForeign('FK24');
        });
    }
}
