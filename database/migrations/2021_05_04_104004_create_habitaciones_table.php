<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHabitacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('habitaciones', function (Blueprint $table) {
            $table->integer('Id', true);
            $table->char('NHabitacion', 3);
            $table->enum('Tipo', ['Superior', 'Normal']);
            $table->enum('TipoBano', ['Banera', 'Plato Ducha']);
            $table->enum('TipoCama', ['Individual', 'Matrimonio']);
            $table->enum('Nivel', ['Alto', 'Bajo']);
            $table->enum('Zona', ['Zona 1', 'Zona 2', 'Zona 3', 'Zona 4']);
            $table->boolean('Mascotas');
            $table->boolean('Discapacitados');
            $table->enum('Estado', ['Disponible', 'Ocupado', 'No Limpio', 'Salida', 'Bloqueado']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('habitaciones');
    }
}
