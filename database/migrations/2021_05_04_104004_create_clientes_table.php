<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->integer('Id', true);
            $table->string('Nombre');
            $table->string('Apellidos');
            $table->string('NumeroIdentificativo')->unique('NumeroIdentificativo');
            $table->string('Email')->nullable();
            $table->string('Telefono', 25)->nullable();
            $table->integer('IdPais')->index('FK3');
            $table->integer('IdComunidadAutonoma')->nullable()->index('FK4');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes');
    }
}
