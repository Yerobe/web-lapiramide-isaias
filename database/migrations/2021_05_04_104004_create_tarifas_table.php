<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTarifasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tarifas', function (Blueprint $table) {
            $table->integer('Id', true);
            $table->string('Nombre');
            $table->enum('TipoRegimen', [1, 2, 3, 4]);
            $table->date('FechaInicio');
            $table->date('FechaFin');
            $table->smallInteger('NumeroAdultos');
            $table->smallInteger('NumeroNinos');
            $table->enum('Temporada', ['Baja', 'Media', 'Alta']);
            $table->float('PrecioNoche', 10, 0);
            $table->boolean('Estado');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tarifas');
    }
}
