<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Empleado;
use Illuminate\Support\Facades\DB;


class EmpleadosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('empleados')->delete();

        Empleado::create(['NIF'=>'78821497Z', 'Nombre'=>'Jerobel', 'Apellidos'=>'Marrero Moreno', 'Email'=>'jmamor2016@gmail.com', 'Contrasena'=>sha1('12345') , 'Rol'=>'Administrador']);
    }
}
