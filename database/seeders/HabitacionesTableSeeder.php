<?php

namespace Database\Seeders;

use App\Models\Habitacion;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HabitacionesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('habitaciones')->delete();


        $habitaciones = array(



            // Apartamentos Bajos


            array('NHabitacion' => '101', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Bajo', 'Zona' => 'Zona 1', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '102', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Bajo', 'Zona' => 'Zona 1', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '103', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Bajo', 'Zona' => 'Zona 1', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '104', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Bajo', 'Zona' => 'Zona 1', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '105', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Bajo', 'Zona' => 'Zona 1', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '106', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Bajo', 'Zona' => 'Zona 1', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '107', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Bajo', 'Zona' => 'Zona 1', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '108', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Bajo', 'Zona' => 'Zona 1', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '109', 'Tipo' => 'Normal', 'TipoBano' => 'Banera', 'TipoCama' => 'Matrimonio', 'Nivel' => 'Bajo', 'Zona' => 'Zona 1', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '110', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Bajo', 'Zona' => 'Zona 1', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),

            array('NHabitacion' => '111', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Bajo', 'Zona' => 'Zona 2', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '112', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Bajo', 'Zona' => 'Zona 2', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '113', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Bajo', 'Zona' => 'Zona 2', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '114', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Bajo', 'Zona' => 'Zona 2', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '115', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Bajo', 'Zona' => 'Zona 2', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '116', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Bajo', 'Zona' => 'Zona 2', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '117', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Bajo', 'Zona' => 'Zona 2', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '118', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Bajo', 'Zona' => 'Zona 2', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '119', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Bajo', 'Zona' => 'Zona 2', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '120', 'Tipo' => 'Superior', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Bajo', 'Zona' => 'Zona 2', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),

            array('NHabitacion' => '121', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Bajo', 'Zona' => 'Zona 3', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '122', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Bajo', 'Zona' => 'Zona 3', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '123', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Bajo', 'Zona' => 'Zona 3', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '124', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Bajo', 'Zona' => 'Zona 3', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '125', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Bajo', 'Zona' => 'Zona 3', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '126', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Bajo', 'Zona' => 'Zona 3', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '127', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Bajo', 'Zona' => 'Zona 3', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '128', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Bajo', 'Zona' => 'Zona 3', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '129', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Bajo', 'Zona' => 'Zona 3', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '130', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Bajo', 'Zona' => 'Zona 3', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '131', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Bajo', 'Zona' => 'Zona 3', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '132', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Bajo', 'Zona' => 'Zona 3', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '133', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Bajo', 'Zona' => 'Zona 3', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '134', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Bajo', 'Zona' => 'Zona 3', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '135', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Bajo', 'Zona' => 'Zona 3', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '136', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Bajo', 'Zona' => 'Zona 3', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),

            array('NHabitacion' => '137', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Bajo', 'Zona' => 'Zona 4', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '138', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Bajo', 'Zona' => 'Zona 4', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '139', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Bajo', 'Zona' => 'Zona 4', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '140', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Bajo', 'Zona' => 'Zona 4', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '141', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Bajo', 'Zona' => 'Zona 4', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '142', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Bajo', 'Zona' => 'Zona 4', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '143', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Bajo', 'Zona' => 'Zona 4', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '144', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Bajo', 'Zona' => 'Zona 4', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '145', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Bajo', 'Zona' => 'Zona 4', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '146', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Bajo', 'Zona' => 'Zona 4', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '147', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Bajo', 'Zona' => 'Zona 4', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '148', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Bajo', 'Zona' => 'Zona 4', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),


            // Apartamentos Altos 


            array('NHabitacion' => '201', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Alto', 'Zona' => 'Zona 1', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '202', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Alto', 'Zona' => 'Zona 1', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '203', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Alto', 'Zona' => 'Zona 1', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '204', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Alto', 'Zona' => 'Zona 1', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '205', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Alto', 'Zona' => 'Zona 1', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '206', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Alto', 'Zona' => 'Zona 1', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '207', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Alto', 'Zona' => 'Zona 1', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '208', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Alto', 'Zona' => 'Zona 1', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '209', 'Tipo' => 'Normal', 'TipoBano' => 'Banera', 'TipoCama' => 'Matrimonio', 'Nivel' => 'Alto', 'Zona' => 'Zona 1', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '210', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Alto', 'Zona' => 'Zona 1', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),

            array('NHabitacion' => '211', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Alto', 'Zona' => 'Zona 2', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '212', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Alto', 'Zona' => 'Zona 2', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '213', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Alto', 'Zona' => 'Zona 2', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '214', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Alto', 'Zona' => 'Zona 2', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '215', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Alto', 'Zona' => 'Zona 2', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '216', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Alto', 'Zona' => 'Zona 2', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '217', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Alto', 'Zona' => 'Zona 2', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '218', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Alto', 'Zona' => 'Zona 2', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '219', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Alto', 'Zona' => 'Zona 2', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '220', 'Tipo' => 'Superior', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Alto', 'Zona' => 'Zona 2', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '221', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Alto', 'Zona' => 'Zona 3', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '222', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Alto', 'Zona' => 'Zona 3', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '223', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Alto', 'Zona' => 'Zona 3', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '224', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Alto', 'Zona' => 'Zona 3', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '225', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Alto', 'Zona' => 'Zona 3', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '226', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Alto', 'Zona' => 'Zona 3', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '227', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Alto', 'Zona' => 'Zona 3', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '228', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Alto', 'Zona' => 'Zona 3', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '229', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Alto', 'Zona' => 'Zona 3', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '230', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Alto', 'Zona' => 'Zona 3', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '231', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Alto', 'Zona' => 'Zona 3', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '232', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Alto', 'Zona' => 'Zona 3', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '233', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Alto', 'Zona' => 'Zona 3', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '234', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Alto', 'Zona' => 'Zona 3', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '235', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Alto', 'Zona' => 'Zona 3', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '236', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Alto', 'Zona' => 'Zona 3', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),

            array('NHabitacion' => '237', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Alto', 'Zona' => 'Zona 4', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '238', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Alto', 'Zona' => 'Zona 4', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '239', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Alto', 'Zona' => 'Zona 4', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '240', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Alto', 'Zona' => 'Zona 4', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '241', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Alto', 'Zona' => 'Zona 4', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '242', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Alto', 'Zona' => 'Zona 4', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '243', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Alto', 'Zona' => 'Zona 4', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '244', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Alto', 'Zona' => 'Zona 4', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '245', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Alto', 'Zona' => 'Zona 4', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '246', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Alto', 'Zona' => 'Zona 4', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '247', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Alto', 'Zona' => 'Zona 4', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),
            array('NHabitacion' => '248', 'Tipo' => 'Normal', 'TipoBano' => 'Plato Ducha', 'TipoCama' => 'Individual', 'Nivel' => 'Alto', 'Zona' => 'Zona 4', 'Mascotas' => 0, 'Discapacitados' => 0, 'Estado' => 'Bloqueado'),

        );

        DB::table('habitaciones')->insert($habitaciones);
    }
}
