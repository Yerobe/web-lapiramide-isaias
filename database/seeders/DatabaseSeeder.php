<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        
        $this->call(HotelesTableSeeder::class);
        // $this->call(EmpleadosTableSeeder::class);
        $this->call(HabitacionesTableSeeder::class);
        $this->call(OperadoresTableSeeder::class);
        $this->call(PaisesTableSeeder::class);
        $this->call(ComunidadesAutonomasTableSeeder::class);
        $this->call(TarifasTableSeeder::class);
        $this->call(EventosTableSeeder::class);
    }
}
