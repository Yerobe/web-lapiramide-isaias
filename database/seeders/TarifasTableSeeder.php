<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TarifasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tarifas')->delete();


        $tarifas = array(

            // ============ Tarifas 1 Persona =============




            // -- 01 de Mayo al 18 de Junio = Temporada Baja

            array('Nombre' => '1 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/05/01', 'FechaFin' => "2021/06/18", 'NumeroAdultos' => 1, 'NumeroNinos' => 0, 'Temporada' => "Baja", 'PrecioNoche' => "34", 'Estado' => 1),
            array('Nombre' => '1 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/05/01', 'FechaFin' => "2021/06/18", 'NumeroAdultos' => 1, 'NumeroNinos' => 1, 'Temporada' => "Baja", 'PrecioNoche' => "39", 'Estado' => 1),
            array('Nombre' => '1 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/05/01', 'FechaFin' => "2021/06/18", 'NumeroAdultos' => 1, 'NumeroNinos' => 2, 'Temporada' => "Baja", 'PrecioNoche' => "44", 'Estado' => 1),
            array('Nombre' => '1 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/05/01', 'FechaFin' => "2021/06/18", 'NumeroAdultos' => 1, 'NumeroNinos' => 3, 'Temporada' => "Baja", 'PrecioNoche' => "49", 'Estado' => 1),
            array('Nombre' => '1 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/05/01', 'FechaFin' => "2021/06/18", 'NumeroAdultos' => 1, 'NumeroNinos' => 4, 'Temporada' => "Baja", 'PrecioNoche' => "54", 'Estado' => 1),

            array('Nombre' => '1 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/05/01', 'FechaFin' => "2021/06/18", 'NumeroAdultos' => 1, 'NumeroNinos' => 0, 'Temporada' => "Baja", 'PrecioNoche' => "39", 'Estado' => 1),
            array('Nombre' => '1 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/05/01', 'FechaFin' => "2021/06/18", 'NumeroAdultos' => 1, 'NumeroNinos' => 1, 'Temporada' => "Baja", 'PrecioNoche' => "44", 'Estado' => 1),
            array('Nombre' => '1 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/05/01', 'FechaFin' => "2021/06/18", 'NumeroAdultos' => 1, 'NumeroNinos' => 2, 'Temporada' => "Baja", 'PrecioNoche' => "49", 'Estado' => 1),
            array('Nombre' => '1 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/05/01', 'FechaFin' => "2021/06/18", 'NumeroAdultos' => 1, 'NumeroNinos' => 3, 'Temporada' => "Baja", 'PrecioNoche' => "54", 'Estado' => 1),
            array('Nombre' => '1 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/05/01', 'FechaFin' => "2021/06/18", 'NumeroAdultos' => 1, 'NumeroNinos' => 4, 'Temporada' => "Baja", 'PrecioNoche' => "59", 'Estado' => 1),


            array('Nombre' => '1 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/05/01', 'FechaFin' => "2021/06/18", 'NumeroAdultos' => 1, 'NumeroNinos' => 0, 'Temporada' => "Baja", 'PrecioNoche' => "47", 'Estado' => 1),
            array('Nombre' => '1 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/05/01', 'FechaFin' => "2021/06/18", 'NumeroAdultos' => 1, 'NumeroNinos' => 1, 'Temporada' => "Baja", 'PrecioNoche' => "52", 'Estado' => 1),
            array('Nombre' => '1 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/05/01', 'FechaFin' => "2021/06/18", 'NumeroAdultos' => 1, 'NumeroNinos' => 2, 'Temporada' => "Baja", 'PrecioNoche' => "62", 'Estado' => 1),
            array('Nombre' => '1 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/05/01', 'FechaFin' => "2021/06/18", 'NumeroAdultos' => 1, 'NumeroNinos' => 3, 'Temporada' => "Baja", 'PrecioNoche' => "67", 'Estado' => 1),
            array('Nombre' => '1 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/05/01', 'FechaFin' => "2021/06/18", 'NumeroAdultos' => 1, 'NumeroNinos' => 4, 'Temporada' => "Baja", 'PrecioNoche' => "72", 'Estado' => 1),



            // -- 13 de Abril al 30 de Abril = Temporada Baja

            array('Nombre' => '1 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/04/13', 'FechaFin' => "2021/04/30", 'NumeroAdultos' => 1, 'NumeroNinos' => 0, 'Temporada' => "Baja", 'PrecioNoche' => "34", 'Estado' => 1),
            array('Nombre' => '1 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/04/13', 'FechaFin' => "2021/04/30", 'NumeroAdultos' => 1, 'NumeroNinos' => 1, 'Temporada' => "Baja", 'PrecioNoche' => "39", 'Estado' => 1),
            array('Nombre' => '1 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/04/13', 'FechaFin' => "2021/04/30", 'NumeroAdultos' => 1, 'NumeroNinos' => 2, 'Temporada' => "Baja", 'PrecioNoche' => "44", 'Estado' => 1),
            array('Nombre' => '1 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/04/13', 'FechaFin' => "2021/04/30", 'NumeroAdultos' => 1, 'NumeroNinos' => 3, 'Temporada' => "Baja", 'PrecioNoche' => "49", 'Estado' => 1),
            array('Nombre' => '1 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/04/13', 'FechaFin' => "2021/04/30", 'NumeroAdultos' => 1, 'NumeroNinos' => 4, 'Temporada' => "Baja", 'PrecioNoche' => "54", 'Estado' => 1),

            array('Nombre' => '1 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/04/13', 'FechaFin' => "2021/04/30", 'NumeroAdultos' => 1, 'NumeroNinos' => 0, 'Temporada' => "Baja", 'PrecioNoche' => "39", 'Estado' => 1),
            array('Nombre' => '1 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/04/13', 'FechaFin' => "2021/04/30", 'NumeroAdultos' => 1, 'NumeroNinos' => 1, 'Temporada' => "Baja", 'PrecioNoche' => "44", 'Estado' => 1),
            array('Nombre' => '1 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/04/13', 'FechaFin' => "2021/04/30", 'NumeroAdultos' => 1, 'NumeroNinos' => 2, 'Temporada' => "Baja", 'PrecioNoche' => "49", 'Estado' => 1),
            array('Nombre' => '1 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/04/13', 'FechaFin' => "2021/04/30", 'NumeroAdultos' => 1, 'NumeroNinos' => 3, 'Temporada' => "Baja", 'PrecioNoche' => "54", 'Estado' => 1),
            array('Nombre' => '1 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/04/13', 'FechaFin' => "2021/04/30", 'NumeroAdultos' => 1, 'NumeroNinos' => 4, 'Temporada' => "Baja", 'PrecioNoche' => "59", 'Estado' => 1),

            array('Nombre' => '1 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/04/13', 'FechaFin' => "2021/04/30", 'NumeroAdultos' => 1, 'NumeroNinos' => 0, 'Temporada' => "Baja", 'PrecioNoche' => "47", 'Estado' => 1),
            array('Nombre' => '1 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/04/13', 'FechaFin' => "2021/04/30", 'NumeroAdultos' => 1, 'NumeroNinos' => 1, 'Temporada' => "Baja", 'PrecioNoche' => "52", 'Estado' => 1),
            array('Nombre' => '1 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/04/13', 'FechaFin' => "2021/04/30", 'NumeroAdultos' => 1, 'NumeroNinos' => 2, 'Temporada' => "Baja", 'PrecioNoche' => "57", 'Estado' => 1),
            array('Nombre' => '1 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/04/13', 'FechaFin' => "2021/04/30", 'NumeroAdultos' => 1, 'NumeroNinos' => 3, 'Temporada' => "Baja", 'PrecioNoche' => "62", 'Estado' => 1),
            array('Nombre' => '1 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/04/13', 'FechaFin' => "2021/04/30", 'NumeroAdultos' => 1, 'NumeroNinos' => 4, 'Temporada' => "Baja", 'PrecioNoche' => "67", 'Estado' => 1),





            // -- 13 de Septiembre al 31 de Octubre = Temporada Media

            array('Nombre' => '1 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/09/13', 'FechaFin' => "2021/10/31", 'NumeroAdultos' => 1, 'NumeroNinos' => 0, 'Temporada' => "Media", 'PrecioNoche' => "40", 'Estado' => 1),
            array('Nombre' => '1 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/09/13', 'FechaFin' => "2021/10/31", 'NumeroAdultos' => 1, 'NumeroNinos' => 1, 'Temporada' => "Media", 'PrecioNoche' => "45", 'Estado' => 1),
            array('Nombre' => '1 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/09/13', 'FechaFin' => "2021/10/31", 'NumeroAdultos' => 1, 'NumeroNinos' => 2, 'Temporada' => "Media", 'PrecioNoche' => "50", 'Estado' => 1),
            array('Nombre' => '1 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/09/13', 'FechaFin' => "2021/10/31", 'NumeroAdultos' => 1, 'NumeroNinos' => 3, 'Temporada' => "Media", 'PrecioNoche' => "55", 'Estado' => 1),
            array('Nombre' => '1 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/09/13', 'FechaFin' => "2021/10/31", 'NumeroAdultos' => 1, 'NumeroNinos' => 4, 'Temporada' => "Media", 'PrecioNoche' => "60", 'Estado' => 1),
            
            array('Nombre' => '1 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/09/13', 'FechaFin' => "2021/10/31", 'NumeroAdultos' => 1, 'NumeroNinos' => 0, 'Temporada' => "Media", 'PrecioNoche' => "45", 'Estado' => 1),
            array('Nombre' => '1 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/09/13', 'FechaFin' => "2021/10/31", 'NumeroAdultos' => 1, 'NumeroNinos' => 1, 'Temporada' => "Media", 'PrecioNoche' => "50", 'Estado' => 1),
            array('Nombre' => '1 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/09/13', 'FechaFin' => "2021/10/31", 'NumeroAdultos' => 1, 'NumeroNinos' => 2, 'Temporada' => "Media", 'PrecioNoche' => "55", 'Estado' => 1),
            array('Nombre' => '1 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/09/13', 'FechaFin' => "2021/10/31", 'NumeroAdultos' => 1, 'NumeroNinos' => 3, 'Temporada' => "Media", 'PrecioNoche' => "60", 'Estado' => 1),
            array('Nombre' => '1 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/09/13', 'FechaFin' => "2021/10/31", 'NumeroAdultos' => 1, 'NumeroNinos' => 4, 'Temporada' => "Media", 'PrecioNoche' => "65", 'Estado' => 1),
            
            
            array('Nombre' => '1 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/09/13', 'FechaFin' => "2021/10/31", 'NumeroAdultos' => 1, 'NumeroNinos' => 0, 'Temporada' => "Media", 'PrecioNoche' => "53", 'Estado' => 1),
            array('Nombre' => '1 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/09/13', 'FechaFin' => "2021/10/31", 'NumeroAdultos' => 1, 'NumeroNinos' => 1, 'Temporada' => "Media", 'PrecioNoche' => "58", 'Estado' => 1),
            array('Nombre' => '1 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/09/13', 'FechaFin' => "2021/10/31", 'NumeroAdultos' => 1, 'NumeroNinos' => 2, 'Temporada' => "Media", 'PrecioNoche' => "63", 'Estado' => 1),
            array('Nombre' => '1 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/09/13', 'FechaFin' => "2021/10/31", 'NumeroAdultos' => 1, 'NumeroNinos' => 3, 'Temporada' => "Media", 'PrecioNoche' => "68", 'Estado' => 1),
            array('Nombre' => '1 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/09/13', 'FechaFin' => "2021/10/31", 'NumeroAdultos' => 1, 'NumeroNinos' => 4, 'Temporada' => "Media", 'PrecioNoche' => "73", 'Estado' => 1),

            // -- 01 de Noviembre al 19 de Diciembre = Temporada Media

            array('Nombre' => '1 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/11/01', 'FechaFin' => "2021/12/19", 'NumeroAdultos' => 1, 'NumeroNinos' => 0, 'Temporada' => "Media", 'PrecioNoche' => "40", 'Estado' => 1),
            array('Nombre' => '1 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/11/01', 'FechaFin' => "2021/12/19", 'NumeroAdultos' => 1, 'NumeroNinos' => 1, 'Temporada' => "Media", 'PrecioNoche' => "45", 'Estado' => 1),
            array('Nombre' => '1 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/11/01', 'FechaFin' => "2021/12/19", 'NumeroAdultos' => 1, 'NumeroNinos' => 2, 'Temporada' => "Media", 'PrecioNoche' => "50", 'Estado' => 1),
            array('Nombre' => '1 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/11/01', 'FechaFin' => "2021/12/19", 'NumeroAdultos' => 1, 'NumeroNinos' => 3, 'Temporada' => "Media", 'PrecioNoche' => "55", 'Estado' => 1),
            array('Nombre' => '1 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/11/01', 'FechaFin' => "2021/12/19", 'NumeroAdultos' => 1, 'NumeroNinos' => 4, 'Temporada' => "Media", 'PrecioNoche' => "60", 'Estado' => 1),
            
            array('Nombre' => '1 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/11/01', 'FechaFin' => "2021/12/19", 'NumeroAdultos' => 1, 'NumeroNinos' => 0, 'Temporada' => "Media", 'PrecioNoche' => "45", 'Estado' => 1),
            array('Nombre' => '1 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/11/01', 'FechaFin' => "2021/12/19", 'NumeroAdultos' => 1, 'NumeroNinos' => 1, 'Temporada' => "Media", 'PrecioNoche' => "50", 'Estado' => 1),
            array('Nombre' => '1 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/11/01', 'FechaFin' => "2021/12/19", 'NumeroAdultos' => 1, 'NumeroNinos' => 2, 'Temporada' => "Media", 'PrecioNoche' => "55", 'Estado' => 1),
            array('Nombre' => '1 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/11/01', 'FechaFin' => "2021/12/19", 'NumeroAdultos' => 1, 'NumeroNinos' => 3, 'Temporada' => "Media", 'PrecioNoche' => "60", 'Estado' => 1),
            array('Nombre' => '1 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/11/01', 'FechaFin' => "2021/12/19", 'NumeroAdultos' => 1, 'NumeroNinos' => 4, 'Temporada' => "Media", 'PrecioNoche' => "65", 'Estado' => 1),
            
            array('Nombre' => '1 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/11/01', 'FechaFin' => "2021/12/19", 'NumeroAdultos' => 1, 'NumeroNinos' => 0, 'Temporada' => "Media", 'PrecioNoche' => "53", 'Estado' => 1),
            array('Nombre' => '1 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/11/01', 'FechaFin' => "2021/12/19", 'NumeroAdultos' => 1, 'NumeroNinos' => 1, 'Temporada' => "Media", 'PrecioNoche' => "58", 'Estado' => 1),
            array('Nombre' => '1 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/11/01', 'FechaFin' => "2021/12/19", 'NumeroAdultos' => 1, 'NumeroNinos' => 2, 'Temporada' => "Media", 'PrecioNoche' => "63", 'Estado' => 1),
            array('Nombre' => '1 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/11/01', 'FechaFin' => "2021/12/19", 'NumeroAdultos' => 1, 'NumeroNinos' => 3, 'Temporada' => "Media", 'PrecioNoche' => "68", 'Estado' => 1),
            array('Nombre' => '1 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/11/01', 'FechaFin' => "2021/12/19", 'NumeroAdultos' => 1, 'NumeroNinos' => 4, 'Temporada' => "Media", 'PrecioNoche' => "73", 'Estado' => 1),


            // -- 06 de Enero al 07 de Abril = Temporada Media

            array('Nombre' => '1 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/01/06', 'FechaFin' => "2021/04/07", 'NumeroAdultos' => 1, 'NumeroNinos' => 0, 'Temporada' => "Media", 'PrecioNoche' => "40", 'Estado' => 1),
            array('Nombre' => '1 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/01/06', 'FechaFin' => "2021/04/07", 'NumeroAdultos' => 1, 'NumeroNinos' => 1, 'Temporada' => "Media", 'PrecioNoche' => "45", 'Estado' => 1),
            array('Nombre' => '1 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/01/06', 'FechaFin' => "2021/04/07", 'NumeroAdultos' => 1, 'NumeroNinos' => 2, 'Temporada' => "Media", 'PrecioNoche' => "50", 'Estado' => 1),
            array('Nombre' => '1 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/01/06', 'FechaFin' => "2021/04/07", 'NumeroAdultos' => 1, 'NumeroNinos' => 3, 'Temporada' => "Media", 'PrecioNoche' => "55", 'Estado' => 1),
            array('Nombre' => '1 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/01/06', 'FechaFin' => "2021/04/07", 'NumeroAdultos' => 1, 'NumeroNinos' => 4, 'Temporada' => "Media", 'PrecioNoche' => "60", 'Estado' => 1),

            array('Nombre' => '1 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/01/06', 'FechaFin' => "2021/04/07", 'NumeroAdultos' => 1, 'NumeroNinos' => 0, 'Temporada' => "Media", 'PrecioNoche' => "45", 'Estado' => 1),
            array('Nombre' => '1 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/01/06', 'FechaFin' => "2021/04/07", 'NumeroAdultos' => 1, 'NumeroNinos' => 1, 'Temporada' => "Media", 'PrecioNoche' => "50", 'Estado' => 1),
            array('Nombre' => '1 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/01/06', 'FechaFin' => "2021/04/07", 'NumeroAdultos' => 1, 'NumeroNinos' => 2, 'Temporada' => "Media", 'PrecioNoche' => "55", 'Estado' => 1),
            array('Nombre' => '1 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/01/06', 'FechaFin' => "2021/04/07", 'NumeroAdultos' => 1, 'NumeroNinos' => 3, 'Temporada' => "Media", 'PrecioNoche' => "60", 'Estado' => 1),
            array('Nombre' => '1 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/01/06', 'FechaFin' => "2021/04/07", 'NumeroAdultos' => 1, 'NumeroNinos' => 4, 'Temporada' => "Media", 'PrecioNoche' => "65", 'Estado' => 1),

            array('Nombre' => '1 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/01/06', 'FechaFin' => "2021/04/07", 'NumeroAdultos' => 1, 'NumeroNinos' => 0, 'Temporada' => "Media", 'PrecioNoche' => "53", 'Estado' => 1),
            array('Nombre' => '1 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/01/06', 'FechaFin' => "2021/04/07", 'NumeroAdultos' => 1, 'NumeroNinos' => 1, 'Temporada' => "Media", 'PrecioNoche' => "58", 'Estado' => 1),
            array('Nombre' => '1 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/01/06', 'FechaFin' => "2021/04/07", 'NumeroAdultos' => 1, 'NumeroNinos' => 2, 'Temporada' => "Media", 'PrecioNoche' => "63", 'Estado' => 1),
            array('Nombre' => '1 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/01/06', 'FechaFin' => "2021/04/07", 'NumeroAdultos' => 1, 'NumeroNinos' => 3, 'Temporada' => "Media", 'PrecioNoche' => "68", 'Estado' => 1),
            array('Nombre' => '1 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/01/06', 'FechaFin' => "2021/04/07", 'NumeroAdultos' => 1, 'NumeroNinos' => 4, 'Temporada' => "Media", 'PrecioNoche' => "73", 'Estado' => 1),






            // -- 19 de Junio al 12 de Septiembre = Temporada Alta

            array('Nombre' => '1 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/06/19', 'FechaFin' => "2021/09/12", 'NumeroAdultos' => 1, 'NumeroNinos' => 0, 'Temporada' => "Alta", 'PrecioNoche' => "50", 'Estado' => 1),
            array('Nombre' => '1 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/06/19', 'FechaFin' => "2021/09/12", 'NumeroAdultos' => 1, 'NumeroNinos' => 1, 'Temporada' => "Alta", 'PrecioNoche' => "55", 'Estado' => 1),
            array('Nombre' => '1 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/06/19', 'FechaFin' => "2021/09/12", 'NumeroAdultos' => 1, 'NumeroNinos' => 2, 'Temporada' => "Alta", 'PrecioNoche' => "60", 'Estado' => 1),
            array('Nombre' => '1 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/06/19', 'FechaFin' => "2021/09/12", 'NumeroAdultos' => 1, 'NumeroNinos' => 3, 'Temporada' => "Alta", 'PrecioNoche' => "65", 'Estado' => 1),
            array('Nombre' => '1 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/06/19', 'FechaFin' => "2021/09/12", 'NumeroAdultos' => 1, 'NumeroNinos' => 4, 'Temporada' => "Alta", 'PrecioNoche' => "70", 'Estado' => 1),

            array('Nombre' => '1 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/06/19', 'FechaFin' => "2021/09/12", 'NumeroAdultos' => 1, 'NumeroNinos' => 0, 'Temporada' => "Alta", 'PrecioNoche' => "55", 'Estado' => 1),
            array('Nombre' => '1 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/06/19', 'FechaFin' => "2021/09/12", 'NumeroAdultos' => 1, 'NumeroNinos' => 1, 'Temporada' => "Alta", 'PrecioNoche' => "60", 'Estado' => 1),
            array('Nombre' => '1 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/06/19', 'FechaFin' => "2021/09/12", 'NumeroAdultos' => 1, 'NumeroNinos' => 2, 'Temporada' => "Alta", 'PrecioNoche' => "65", 'Estado' => 1),
            array('Nombre' => '1 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/06/19', 'FechaFin' => "2021/09/12", 'NumeroAdultos' => 1, 'NumeroNinos' => 3, 'Temporada' => "Alta", 'PrecioNoche' => "70", 'Estado' => 1),
            array('Nombre' => '1 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/06/19', 'FechaFin' => "2021/09/12", 'NumeroAdultos' => 1, 'NumeroNinos' => 4, 'Temporada' => "Alta", 'PrecioNoche' => "75", 'Estado' => 1),

            array('Nombre' => '1 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/06/19', 'FechaFin' => "2021/09/12", 'NumeroAdultos' => 1, 'NumeroNinos' => 0, 'Temporada' => "Alta", 'PrecioNoche' => "63", 'Estado' => 1),
            array('Nombre' => '1 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/06/19', 'FechaFin' => "2021/09/12", 'NumeroAdultos' => 1, 'NumeroNinos' => 1, 'Temporada' => "Alta", 'PrecioNoche' => "68", 'Estado' => 1),
            array('Nombre' => '1 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/06/19', 'FechaFin' => "2021/09/12", 'NumeroAdultos' => 1, 'NumeroNinos' => 2, 'Temporada' => "Alta", 'PrecioNoche' => "73", 'Estado' => 1),
            array('Nombre' => '1 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/06/19', 'FechaFin' => "2021/09/12", 'NumeroAdultos' => 1, 'NumeroNinos' => 3, 'Temporada' => "Alta", 'PrecioNoche' => "78", 'Estado' => 1),
            array('Nombre' => '1 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/06/19', 'FechaFin' => "2021/09/12", 'NumeroAdultos' => 1, 'NumeroNinos' => 4, 'Temporada' => "Alta", 'PrecioNoche' => "83", 'Estado' => 1),


            // -- 20 de Diciembre al 31 de Diciembre = Temporada Alta

            array('Nombre' => '1 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/12/20', 'FechaFin' => "2021/12/31", 'NumeroAdultos' => 1, 'NumeroNinos' => 0, 'Temporada' => "Alta", 'PrecioNoche' => "50", 'Estado' => 1),
            array('Nombre' => '1 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/12/20', 'FechaFin' => "2021/12/31", 'NumeroAdultos' => 1, 'NumeroNinos' => 1, 'Temporada' => "Alta", 'PrecioNoche' => "55", 'Estado' => 1),
            array('Nombre' => '1 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/12/20', 'FechaFin' => "2021/12/31", 'NumeroAdultos' => 1, 'NumeroNinos' => 2, 'Temporada' => "Alta", 'PrecioNoche' => "60", 'Estado' => 1),
            array('Nombre' => '1 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/12/20', 'FechaFin' => "2021/12/31", 'NumeroAdultos' => 1, 'NumeroNinos' => 3, 'Temporada' => "Alta", 'PrecioNoche' => "65", 'Estado' => 1),
            array('Nombre' => '1 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/12/20', 'FechaFin' => "2021/12/31", 'NumeroAdultos' => 1, 'NumeroNinos' => 4, 'Temporada' => "Alta", 'PrecioNoche' => "70", 'Estado' => 1),
            
            array('Nombre' => '1 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/12/20', 'FechaFin' => "2021/12/31", 'NumeroAdultos' => 1, 'NumeroNinos' => 0, 'Temporada' => "Alta", 'PrecioNoche' => "55", 'Estado' => 1),
            array('Nombre' => '1 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/12/20', 'FechaFin' => "2021/12/31", 'NumeroAdultos' => 1, 'NumeroNinos' => 1, 'Temporada' => "Alta", 'PrecioNoche' => "60", 'Estado' => 1),
            array('Nombre' => '1 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/12/20', 'FechaFin' => "2021/12/31", 'NumeroAdultos' => 1, 'NumeroNinos' => 2, 'Temporada' => "Alta", 'PrecioNoche' => "65", 'Estado' => 1),
            array('Nombre' => '1 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/12/20', 'FechaFin' => "2021/12/31", 'NumeroAdultos' => 1, 'NumeroNinos' => 3, 'Temporada' => "Alta", 'PrecioNoche' => "70", 'Estado' => 1),
            array('Nombre' => '1 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/12/20', 'FechaFin' => "2021/12/31", 'NumeroAdultos' => 1, 'NumeroNinos' => 4, 'Temporada' => "Alta", 'PrecioNoche' => "75", 'Estado' => 1),
            
            array('Nombre' => '1 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/12/20', 'FechaFin' => "2021/12/31", 'NumeroAdultos' => 1, 'NumeroNinos' => 0, 'Temporada' => "Alta", 'PrecioNoche' => "63", 'Estado' => 1),
            array('Nombre' => '1 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/12/20', 'FechaFin' => "2021/12/31", 'NumeroAdultos' => 1, 'NumeroNinos' => 1, 'Temporada' => "Alta", 'PrecioNoche' => "68", 'Estado' => 1),
            array('Nombre' => '1 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/12/20', 'FechaFin' => "2021/12/31", 'NumeroAdultos' => 1, 'NumeroNinos' => 2, 'Temporada' => "Alta", 'PrecioNoche' => "73", 'Estado' => 1),
            array('Nombre' => '1 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/12/20', 'FechaFin' => "2021/12/31", 'NumeroAdultos' => 1, 'NumeroNinos' => 3, 'Temporada' => "Alta", 'PrecioNoche' => "78", 'Estado' => 1),
            array('Nombre' => '1 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/12/20', 'FechaFin' => "2021/12/31", 'NumeroAdultos' => 1, 'NumeroNinos' => 4, 'Temporada' => "Alta", 'PrecioNoche' => "83", 'Estado' => 1),

            
            // -- 01 de Enero al 05 de Enero = Temporada Alta

            array('Nombre' => '1 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/01/01', 'FechaFin' => "2021/01/05", 'NumeroAdultos' => 1, 'NumeroNinos' => 0, 'Temporada' => "Alta", 'PrecioNoche' => "50", 'Estado' => 1),
            array('Nombre' => '1 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/01/01', 'FechaFin' => "2021/01/05", 'NumeroAdultos' => 1, 'NumeroNinos' => 1, 'Temporada' => "Alta", 'PrecioNoche' => "55", 'Estado' => 1),
            array('Nombre' => '1 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/01/01', 'FechaFin' => "2021/01/05", 'NumeroAdultos' => 1, 'NumeroNinos' => 2, 'Temporada' => "Alta", 'PrecioNoche' => "60", 'Estado' => 1),
            array('Nombre' => '1 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/01/01', 'FechaFin' => "2021/01/05", 'NumeroAdultos' => 1, 'NumeroNinos' => 3, 'Temporada' => "Alta", 'PrecioNoche' => "65", 'Estado' => 1),
            array('Nombre' => '1 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/01/01', 'FechaFin' => "2021/01/05", 'NumeroAdultos' => 1, 'NumeroNinos' => 4, 'Temporada' => "Alta", 'PrecioNoche' => "70", 'Estado' => 1),

            array('Nombre' => '1 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/01/01', 'FechaFin' => "2021/01/05", 'NumeroAdultos' => 1, 'NumeroNinos' => 0, 'Temporada' => "Alta", 'PrecioNoche' => "55", 'Estado' => 1),
            array('Nombre' => '1 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/01/01', 'FechaFin' => "2021/01/05", 'NumeroAdultos' => 1, 'NumeroNinos' => 1, 'Temporada' => "Alta", 'PrecioNoche' => "60", 'Estado' => 1),
            array('Nombre' => '1 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/01/01', 'FechaFin' => "2021/01/05", 'NumeroAdultos' => 1, 'NumeroNinos' => 2, 'Temporada' => "Alta", 'PrecioNoche' => "65", 'Estado' => 1),
            array('Nombre' => '1 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/01/01', 'FechaFin' => "2021/01/05", 'NumeroAdultos' => 1, 'NumeroNinos' => 3, 'Temporada' => "Alta", 'PrecioNoche' => "70", 'Estado' => 1),
            array('Nombre' => '1 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/01/01', 'FechaFin' => "2021/01/05", 'NumeroAdultos' => 1, 'NumeroNinos' => 4, 'Temporada' => "Alta", 'PrecioNoche' => "75", 'Estado' => 1),

            array('Nombre' => '1 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/01/01', 'FechaFin' => "2021/01/05", 'NumeroAdultos' => 1, 'NumeroNinos' => 0, 'Temporada' => "Alta", 'PrecioNoche' => "63", 'Estado' => 1),
            array('Nombre' => '1 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/01/01', 'FechaFin' => "2021/01/05", 'NumeroAdultos' => 1, 'NumeroNinos' => 1, 'Temporada' => "Alta", 'PrecioNoche' => "68", 'Estado' => 1),
            array('Nombre' => '1 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/01/01', 'FechaFin' => "2021/01/05", 'NumeroAdultos' => 1, 'NumeroNinos' => 2, 'Temporada' => "Alta", 'PrecioNoche' => "73", 'Estado' => 1),
            array('Nombre' => '1 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/01/01', 'FechaFin' => "2021/01/05", 'NumeroAdultos' => 1, 'NumeroNinos' => 3, 'Temporada' => "Alta", 'PrecioNoche' => "78", 'Estado' => 1),
            array('Nombre' => '1 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/01/01', 'FechaFin' => "2021/01/05", 'NumeroAdultos' => 1, 'NumeroNinos' => 4, 'Temporada' => "Alta", 'PrecioNoche' => "83", 'Estado' => 1),



            // -- 08 de Abril al 12 de Abril = Temporada Alta

            array('Nombre' => '1 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/04/08', 'FechaFin' => "2021/04/12", 'NumeroAdultos' => 1, 'NumeroNinos' => 0, 'Temporada' => "Alta", 'PrecioNoche' => "50", 'Estado' => 1),
            array('Nombre' => '1 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/04/08', 'FechaFin' => "2021/04/12", 'NumeroAdultos' => 1, 'NumeroNinos' => 1, 'Temporada' => "Alta", 'PrecioNoche' => "55", 'Estado' => 1),
            array('Nombre' => '1 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/04/08', 'FechaFin' => "2021/04/12", 'NumeroAdultos' => 1, 'NumeroNinos' => 2, 'Temporada' => "Alta", 'PrecioNoche' => "60", 'Estado' => 1),
            array('Nombre' => '1 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/04/08', 'FechaFin' => "2021/04/12", 'NumeroAdultos' => 1, 'NumeroNinos' => 3, 'Temporada' => "Alta", 'PrecioNoche' => "65", 'Estado' => 1),
            array('Nombre' => '1 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/04/08', 'FechaFin' => "2021/04/12", 'NumeroAdultos' => 1, 'NumeroNinos' => 4, 'Temporada' => "Alta", 'PrecioNoche' => "70", 'Estado' => 1),

            array('Nombre' => '1 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/04/08', 'FechaFin' => "2021/04/12", 'NumeroAdultos' => 1, 'NumeroNinos' => 0, 'Temporada' => "Alta", 'PrecioNoche' => "55", 'Estado' => 1),
            array('Nombre' => '1 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/04/08', 'FechaFin' => "2021/04/12", 'NumeroAdultos' => 1, 'NumeroNinos' => 1, 'Temporada' => "Alta", 'PrecioNoche' => "60", 'Estado' => 1),
            array('Nombre' => '1 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/04/08', 'FechaFin' => "2021/04/12", 'NumeroAdultos' => 1, 'NumeroNinos' => 2, 'Temporada' => "Alta", 'PrecioNoche' => "65", 'Estado' => 1),
            array('Nombre' => '1 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/04/08', 'FechaFin' => "2021/04/12", 'NumeroAdultos' => 1, 'NumeroNinos' => 3, 'Temporada' => "Alta", 'PrecioNoche' => "70", 'Estado' => 1),
            array('Nombre' => '1 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/04/08', 'FechaFin' => "2021/04/12", 'NumeroAdultos' => 1, 'NumeroNinos' => 4, 'Temporada' => "Alta", 'PrecioNoche' => "75", 'Estado' => 1),
            
            array('Nombre' => '1 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/04/08', 'FechaFin' => "2021/04/12", 'NumeroAdultos' => 1, 'NumeroNinos' => 0, 'Temporada' => "Alta", 'PrecioNoche' => "63", 'Estado' => 1),
            array('Nombre' => '1 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/04/08', 'FechaFin' => "2021/04/12", 'NumeroAdultos' => 1, 'NumeroNinos' => 1, 'Temporada' => "Alta", 'PrecioNoche' => "68", 'Estado' => 1),
            array('Nombre' => '1 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/04/08', 'FechaFin' => "2021/04/12", 'NumeroAdultos' => 1, 'NumeroNinos' => 2, 'Temporada' => "Alta", 'PrecioNoche' => "73", 'Estado' => 1),
            array('Nombre' => '1 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/04/08', 'FechaFin' => "2021/04/12", 'NumeroAdultos' => 1, 'NumeroNinos' => 3, 'Temporada' => "Alta", 'PrecioNoche' => "78", 'Estado' => 1),
            array('Nombre' => '1 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/04/08', 'FechaFin' => "2021/04/12", 'NumeroAdultos' => 1, 'NumeroNinos' => 4, 'Temporada' => "Alta", 'PrecioNoche' => "83", 'Estado' => 1),


            // ============ Tarifas 1 Persona =============



            
            // ============ Tarifas 2 Persona =============




            // -- 01 de Mayo al 18 de Junio = Temporada Baja

            array('Nombre' => '2 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/05/01', 'FechaFin' => "2021/06/18", 'NumeroAdultos' => 2, 'NumeroNinos' => 0, 'Temporada' => "Baja", 'PrecioNoche' => "38", 'Estado' => 1),
            array('Nombre' => '2 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/05/01', 'FechaFin' => "2021/06/18", 'NumeroAdultos' => 2, 'NumeroNinos' => 1, 'Temporada' => "Baja", 'PrecioNoche' => "43", 'Estado' => 1),
            array('Nombre' => '2 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/05/01', 'FechaFin' => "2021/06/18", 'NumeroAdultos' => 2, 'NumeroNinos' => 2, 'Temporada' => "Baja", 'PrecioNoche' => "48", 'Estado' => 1),
            array('Nombre' => '2 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/05/01', 'FechaFin' => "2021/06/18", 'NumeroAdultos' => 2, 'NumeroNinos' => 3, 'Temporada' => "Baja", 'PrecioNoche' => "53", 'Estado' => 1),
            array('Nombre' => '2 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/05/01', 'FechaFin' => "2021/06/18", 'NumeroAdultos' => 2, 'NumeroNinos' => 4, 'Temporada' => "Baja", 'PrecioNoche' => "58", 'Estado' => 1),

            array('Nombre' => '2 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/05/01', 'FechaFin' => "2021/06/18", 'NumeroAdultos' => 2, 'NumeroNinos' => 0, 'Temporada' => "Baja", 'PrecioNoche' => "46", 'Estado' => 1),
            array('Nombre' => '2 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/05/01', 'FechaFin' => "2021/06/18", 'NumeroAdultos' => 2, 'NumeroNinos' => 1, 'Temporada' => "Baja", 'PrecioNoche' => "51", 'Estado' => 1),
            array('Nombre' => '2 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/05/01', 'FechaFin' => "2021/06/18", 'NumeroAdultos' => 2, 'NumeroNinos' => 2, 'Temporada' => "Baja", 'PrecioNoche' => "56", 'Estado' => 1),
            array('Nombre' => '2 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/05/01', 'FechaFin' => "2021/06/18", 'NumeroAdultos' => 2, 'NumeroNinos' => 3, 'Temporada' => "Baja", 'PrecioNoche' => "61", 'Estado' => 1),
            array('Nombre' => '2 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/05/01', 'FechaFin' => "2021/06/18", 'NumeroAdultos' => 2, 'NumeroNinos' => 4, 'Temporada' => "Baja", 'PrecioNoche' => "66", 'Estado' => 1),


            array('Nombre' => '2 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/05/01', 'FechaFin' => "2021/06/18", 'NumeroAdultos' => 2, 'NumeroNinos' => 0, 'Temporada' => "Baja", 'PrecioNoche' => "62", 'Estado' => 1),
            array('Nombre' => '2 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/05/01', 'FechaFin' => "2021/06/18", 'NumeroAdultos' => 2, 'NumeroNinos' => 1, 'Temporada' => "Baja", 'PrecioNoche' => "67", 'Estado' => 1),
            array('Nombre' => '2 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/05/01', 'FechaFin' => "2021/06/18", 'NumeroAdultos' => 2, 'NumeroNinos' => 2, 'Temporada' => "Baja", 'PrecioNoche' => "72", 'Estado' => 1),
            array('Nombre' => '2 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/05/01', 'FechaFin' => "2021/06/18", 'NumeroAdultos' => 2, 'NumeroNinos' => 3, 'Temporada' => "Baja", 'PrecioNoche' => "77", 'Estado' => 1),
            array('Nombre' => '2 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/05/01', 'FechaFin' => "2021/06/18", 'NumeroAdultos' => 2, 'NumeroNinos' => 4, 'Temporada' => "Baja", 'PrecioNoche' => "82", 'Estado' => 1),



            // -- 13 de Abril al 30 de Abril = Temporada Baja

            array('Nombre' => '2 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/04/13', 'FechaFin' => "2021/04/30", 'NumeroAdultos' => 2, 'NumeroNinos' => 0, 'Temporada' => "Baja", 'PrecioNoche' => "38", 'Estado' => 1),
            array('Nombre' => '2 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/04/13', 'FechaFin' => "2021/04/30", 'NumeroAdultos' => 2, 'NumeroNinos' => 1, 'Temporada' => "Baja", 'PrecioNoche' => "43", 'Estado' => 1),
            array('Nombre' => '2 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/04/13', 'FechaFin' => "2021/04/30", 'NumeroAdultos' => 2, 'NumeroNinos' => 2, 'Temporada' => "Baja", 'PrecioNoche' => "48", 'Estado' => 1),
            array('Nombre' => '2 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/04/13', 'FechaFin' => "2021/04/30", 'NumeroAdultos' => 2, 'NumeroNinos' => 3, 'Temporada' => "Baja", 'PrecioNoche' => "53", 'Estado' => 1),
            array('Nombre' => '2 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/04/13', 'FechaFin' => "2021/04/30", 'NumeroAdultos' => 2, 'NumeroNinos' => 4, 'Temporada' => "Baja", 'PrecioNoche' => "58", 'Estado' => 1),

            array('Nombre' => '2 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/04/13', 'FechaFin' => "2021/04/30", 'NumeroAdultos' => 2, 'NumeroNinos' => 0, 'Temporada' => "Baja", 'PrecioNoche' => "46", 'Estado' => 1),
            array('Nombre' => '2 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/04/13', 'FechaFin' => "2021/04/30", 'NumeroAdultos' => 2, 'NumeroNinos' => 1, 'Temporada' => "Baja", 'PrecioNoche' => "51", 'Estado' => 1),
            array('Nombre' => '2 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/04/13', 'FechaFin' => "2021/04/30", 'NumeroAdultos' => 2, 'NumeroNinos' => 2, 'Temporada' => "Baja", 'PrecioNoche' => "56", 'Estado' => 1),
            array('Nombre' => '2 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/04/13', 'FechaFin' => "2021/04/30", 'NumeroAdultos' => 2, 'NumeroNinos' => 3, 'Temporada' => "Baja", 'PrecioNoche' => "61", 'Estado' => 1),
            array('Nombre' => '2 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/04/13', 'FechaFin' => "2021/04/30", 'NumeroAdultos' => 2, 'NumeroNinos' => 4, 'Temporada' => "Baja", 'PrecioNoche' => "66", 'Estado' => 1),

            array('Nombre' => '2 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/04/13', 'FechaFin' => "2021/04/30", 'NumeroAdultos' => 2, 'NumeroNinos' => 0, 'Temporada' => "Baja", 'PrecioNoche' => "62", 'Estado' => 1),
            array('Nombre' => '2 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/04/13', 'FechaFin' => "2021/04/30", 'NumeroAdultos' => 2, 'NumeroNinos' => 1, 'Temporada' => "Baja", 'PrecioNoche' => "67", 'Estado' => 1),
            array('Nombre' => '2 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/04/13', 'FechaFin' => "2021/04/30", 'NumeroAdultos' => 2, 'NumeroNinos' => 2, 'Temporada' => "Baja", 'PrecioNoche' => "72", 'Estado' => 1),
            array('Nombre' => '2 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/04/13', 'FechaFin' => "2021/04/30", 'NumeroAdultos' => 2, 'NumeroNinos' => 3, 'Temporada' => "Baja", 'PrecioNoche' => "77", 'Estado' => 1),
            array('Nombre' => '2 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/04/13', 'FechaFin' => "2021/04/30", 'NumeroAdultos' => 2, 'NumeroNinos' => 4, 'Temporada' => "Baja", 'PrecioNoche' => "82", 'Estado' => 1),





            // -- 13 de Septiembre al 31 de Octubre = Temporada Media

            array('Nombre' => '2 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/09/13', 'FechaFin' => "2021/10/31", 'NumeroAdultos' => 2, 'NumeroNinos' => 0, 'Temporada' => "Media", 'PrecioNoche' => "45", 'Estado' => 1),
            array('Nombre' => '2 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/09/13', 'FechaFin' => "2021/10/31", 'NumeroAdultos' => 2, 'NumeroNinos' => 1, 'Temporada' => "Media", 'PrecioNoche' => "50", 'Estado' => 1),
            array('Nombre' => '2 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/09/13', 'FechaFin' => "2021/10/31", 'NumeroAdultos' => 2, 'NumeroNinos' => 2, 'Temporada' => "Media", 'PrecioNoche' => "55", 'Estado' => 1),
            array('Nombre' => '2 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/09/13', 'FechaFin' => "2021/10/31", 'NumeroAdultos' => 2, 'NumeroNinos' => 3, 'Temporada' => "Media", 'PrecioNoche' => "60", 'Estado' => 1),
            array('Nombre' => '2 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/09/13', 'FechaFin' => "2021/10/31", 'NumeroAdultos' => 2, 'NumeroNinos' => 4, 'Temporada' => "Media", 'PrecioNoche' => "65", 'Estado' => 1),
            
            array('Nombre' => '2 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/09/13', 'FechaFin' => "2021/10/31", 'NumeroAdultos' => 2, 'NumeroNinos' => 0, 'Temporada' => "Media", 'PrecioNoche' => "51", 'Estado' => 1),
            array('Nombre' => '2 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/09/13', 'FechaFin' => "2021/10/31", 'NumeroAdultos' => 2, 'NumeroNinos' => 1, 'Temporada' => "Media", 'PrecioNoche' => "56", 'Estado' => 1),
            array('Nombre' => '2 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/09/13', 'FechaFin' => "2021/10/31", 'NumeroAdultos' => 2, 'NumeroNinos' => 2, 'Temporada' => "Media", 'PrecioNoche' => "61", 'Estado' => 1),
            array('Nombre' => '2 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/09/13', 'FechaFin' => "2021/10/31", 'NumeroAdultos' => 2, 'NumeroNinos' => 3, 'Temporada' => "Media", 'PrecioNoche' => "66", 'Estado' => 1),
            array('Nombre' => '2 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/09/13', 'FechaFin' => "2021/10/31", 'NumeroAdultos' => 2, 'NumeroNinos' => 4, 'Temporada' => "Media", 'PrecioNoche' => "71", 'Estado' => 1),
            
            
            array('Nombre' => '2 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/09/13', 'FechaFin' => "2021/10/31", 'NumeroAdultos' => 2, 'NumeroNinos' => 0, 'Temporada' => "Media", 'PrecioNoche' => "67", 'Estado' => 1),
            array('Nombre' => '2 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/09/13', 'FechaFin' => "2021/10/31", 'NumeroAdultos' => 2, 'NumeroNinos' => 1, 'Temporada' => "Media", 'PrecioNoche' => "72", 'Estado' => 1),
            array('Nombre' => '2 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/09/13', 'FechaFin' => "2021/10/31", 'NumeroAdultos' => 2, 'NumeroNinos' => 2, 'Temporada' => "Media", 'PrecioNoche' => "77", 'Estado' => 1),
            array('Nombre' => '2 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/09/13', 'FechaFin' => "2021/10/31", 'NumeroAdultos' => 2, 'NumeroNinos' => 3, 'Temporada' => "Media", 'PrecioNoche' => "82", 'Estado' => 1),
            array('Nombre' => '2 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/09/13', 'FechaFin' => "2021/10/31", 'NumeroAdultos' => 2, 'NumeroNinos' => 4, 'Temporada' => "Media", 'PrecioNoche' => "87", 'Estado' => 1),

            // -- 01 de Noviembre al 19 de Diciembre = Temporada Media

            array('Nombre' => '2 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/11/01', 'FechaFin' => "2021/12/19", 'NumeroAdultos' => 2, 'NumeroNinos' => 0, 'Temporada' => "Media", 'PrecioNoche' => "45", 'Estado' => 1),
            array('Nombre' => '2 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/11/01', 'FechaFin' => "2021/12/19", 'NumeroAdultos' => 2, 'NumeroNinos' => 1, 'Temporada' => "Media", 'PrecioNoche' => "50", 'Estado' => 1),
            array('Nombre' => '2 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/11/01', 'FechaFin' => "2021/12/19", 'NumeroAdultos' => 2, 'NumeroNinos' => 2, 'Temporada' => "Media", 'PrecioNoche' => "55", 'Estado' => 1),
            array('Nombre' => '2 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/11/01', 'FechaFin' => "2021/12/19", 'NumeroAdultos' => 2, 'NumeroNinos' => 3, 'Temporada' => "Media", 'PrecioNoche' => "60", 'Estado' => 1),
            array('Nombre' => '2 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/11/01', 'FechaFin' => "2021/12/19", 'NumeroAdultos' => 2, 'NumeroNinos' => 4, 'Temporada' => "Media", 'PrecioNoche' => "65", 'Estado' => 1),
            
            array('Nombre' => '2 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/11/01', 'FechaFin' => "2021/12/19", 'NumeroAdultos' => 2, 'NumeroNinos' => 0, 'Temporada' => "Media", 'PrecioNoche' => "51", 'Estado' => 1),
            array('Nombre' => '2 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/11/01', 'FechaFin' => "2021/12/19", 'NumeroAdultos' => 2, 'NumeroNinos' => 1, 'Temporada' => "Media", 'PrecioNoche' => "56", 'Estado' => 1),
            array('Nombre' => '2 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/11/01', 'FechaFin' => "2021/12/19", 'NumeroAdultos' => 2, 'NumeroNinos' => 2, 'Temporada' => "Media", 'PrecioNoche' => "61", 'Estado' => 1),
            array('Nombre' => '2 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/11/01', 'FechaFin' => "2021/12/19", 'NumeroAdultos' => 2, 'NumeroNinos' => 3, 'Temporada' => "Media", 'PrecioNoche' => "66", 'Estado' => 1),
            array('Nombre' => '2 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/11/01', 'FechaFin' => "2021/12/19", 'NumeroAdultos' => 2, 'NumeroNinos' => 4, 'Temporada' => "Media", 'PrecioNoche' => "71", 'Estado' => 1),
            
            array('Nombre' => '2 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/11/01', 'FechaFin' => "2021/12/19", 'NumeroAdultos' => 2, 'NumeroNinos' => 0, 'Temporada' => "Media", 'PrecioNoche' => "67", 'Estado' => 1),
            array('Nombre' => '2 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/11/01', 'FechaFin' => "2021/12/19", 'NumeroAdultos' => 2, 'NumeroNinos' => 1, 'Temporada' => "Media", 'PrecioNoche' => "72", 'Estado' => 1),
            array('Nombre' => '2 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/11/01', 'FechaFin' => "2021/12/19", 'NumeroAdultos' => 2, 'NumeroNinos' => 2, 'Temporada' => "Media", 'PrecioNoche' => "77", 'Estado' => 1),
            array('Nombre' => '2 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/11/01', 'FechaFin' => "2021/12/19", 'NumeroAdultos' => 2, 'NumeroNinos' => 3, 'Temporada' => "Media", 'PrecioNoche' => "82", 'Estado' => 1),
            array('Nombre' => '2 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/11/01', 'FechaFin' => "2021/12/19", 'NumeroAdultos' => 2, 'NumeroNinos' => 4, 'Temporada' => "Media", 'PrecioNoche' => "87", 'Estado' => 1),


            // -- 06 de Enero al 07 de Abril = Temporada Media

            array('Nombre' => '2 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/01/06', 'FechaFin' => "2021/04/07", 'NumeroAdultos' => 2, 'NumeroNinos' => 0, 'Temporada' => "Media", 'PrecioNoche' => "45", 'Estado' => 1),
            array('Nombre' => '2 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/01/06', 'FechaFin' => "2021/04/07", 'NumeroAdultos' => 2, 'NumeroNinos' => 1, 'Temporada' => "Media", 'PrecioNoche' => "50", 'Estado' => 1),
            array('Nombre' => '2 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/01/06', 'FechaFin' => "2021/04/07", 'NumeroAdultos' => 2, 'NumeroNinos' => 2, 'Temporada' => "Media", 'PrecioNoche' => "55", 'Estado' => 1),
            array('Nombre' => '2 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/01/06', 'FechaFin' => "2021/04/07", 'NumeroAdultos' => 2, 'NumeroNinos' => 3, 'Temporada' => "Media", 'PrecioNoche' => "60", 'Estado' => 1),
            array('Nombre' => '2 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/01/06', 'FechaFin' => "2021/04/07", 'NumeroAdultos' => 2, 'NumeroNinos' => 4, 'Temporada' => "Media", 'PrecioNoche' => "65", 'Estado' => 1),

            array('Nombre' => '2 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/01/06', 'FechaFin' => "2021/04/07", 'NumeroAdultos' => 2, 'NumeroNinos' => 0, 'Temporada' => "Media", 'PrecioNoche' => "51", 'Estado' => 1),
            array('Nombre' => '2 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/01/06', 'FechaFin' => "2021/04/07", 'NumeroAdultos' => 2, 'NumeroNinos' => 1, 'Temporada' => "Media", 'PrecioNoche' => "56", 'Estado' => 1),
            array('Nombre' => '2 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/01/06', 'FechaFin' => "2021/04/07", 'NumeroAdultos' => 2, 'NumeroNinos' => 2, 'Temporada' => "Media", 'PrecioNoche' => "61", 'Estado' => 1),
            array('Nombre' => '2 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/01/06', 'FechaFin' => "2021/04/07", 'NumeroAdultos' => 2, 'NumeroNinos' => 3, 'Temporada' => "Media", 'PrecioNoche' => "66", 'Estado' => 1),
            array('Nombre' => '2 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/01/06', 'FechaFin' => "2021/04/07", 'NumeroAdultos' => 2, 'NumeroNinos' => 4, 'Temporada' => "Media", 'PrecioNoche' => "71", 'Estado' => 1),

            array('Nombre' => '2 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/01/06', 'FechaFin' => "2021/04/07", 'NumeroAdultos' => 2, 'NumeroNinos' => 0, 'Temporada' => "Media", 'PrecioNoche' => "67", 'Estado' => 1),
            array('Nombre' => '2 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/01/06', 'FechaFin' => "2021/04/07", 'NumeroAdultos' => 2, 'NumeroNinos' => 1, 'Temporada' => "Media", 'PrecioNoche' => "72", 'Estado' => 1),
            array('Nombre' => '2 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/01/06', 'FechaFin' => "2021/04/07", 'NumeroAdultos' => 2, 'NumeroNinos' => 2, 'Temporada' => "Media", 'PrecioNoche' => "77", 'Estado' => 1),
            array('Nombre' => '2 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/01/06', 'FechaFin' => "2021/04/07", 'NumeroAdultos' => 2, 'NumeroNinos' => 3, 'Temporada' => "Media", 'PrecioNoche' => "82", 'Estado' => 1),
            array('Nombre' => '2 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/01/06', 'FechaFin' => "2021/04/07", 'NumeroAdultos' => 2, 'NumeroNinos' => 4, 'Temporada' => "Media", 'PrecioNoche' => "87", 'Estado' => 1),






            // -- 19 de Junio al 12 de Septiembre = Temporada Alta

            array('Nombre' => '2 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/06/19', 'FechaFin' => "2021/09/12", 'NumeroAdultos' => 2, 'NumeroNinos' => 0, 'Temporada' => "Alta", 'PrecioNoche' => "55", 'Estado' => 1),
            array('Nombre' => '2 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/06/19', 'FechaFin' => "2021/09/12", 'NumeroAdultos' => 2, 'NumeroNinos' => 1, 'Temporada' => "Alta", 'PrecioNoche' => "60", 'Estado' => 1),
            array('Nombre' => '2 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/06/19', 'FechaFin' => "2021/09/12", 'NumeroAdultos' => 2, 'NumeroNinos' => 2, 'Temporada' => "Alta", 'PrecioNoche' => "65", 'Estado' => 1),
            array('Nombre' => '2 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/06/19', 'FechaFin' => "2021/09/12", 'NumeroAdultos' => 2, 'NumeroNinos' => 3, 'Temporada' => "Alta", 'PrecioNoche' => "70", 'Estado' => 1),
            array('Nombre' => '2 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/06/19', 'FechaFin' => "2021/09/12", 'NumeroAdultos' => 2, 'NumeroNinos' => 4, 'Temporada' => "Alta", 'PrecioNoche' => "75", 'Estado' => 1),

            array('Nombre' => '2 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/06/19', 'FechaFin' => "2021/09/12", 'NumeroAdultos' => 2, 'NumeroNinos' => 0, 'Temporada' => "Alta", 'PrecioNoche' => "63", 'Estado' => 1),
            array('Nombre' => '2 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/06/19', 'FechaFin' => "2021/09/12", 'NumeroAdultos' => 2, 'NumeroNinos' => 1, 'Temporada' => "Alta", 'PrecioNoche' => "68", 'Estado' => 1),
            array('Nombre' => '2 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/06/19', 'FechaFin' => "2021/09/12", 'NumeroAdultos' => 2, 'NumeroNinos' => 2, 'Temporada' => "Alta", 'PrecioNoche' => "73", 'Estado' => 1),
            array('Nombre' => '2 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/06/19', 'FechaFin' => "2021/09/12", 'NumeroAdultos' => 2, 'NumeroNinos' => 3, 'Temporada' => "Alta", 'PrecioNoche' => "78", 'Estado' => 1),
            array('Nombre' => '2 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/06/19', 'FechaFin' => "2021/09/12", 'NumeroAdultos' => 2, 'NumeroNinos' => 4, 'Temporada' => "Alta", 'PrecioNoche' => "83", 'Estado' => 1),

            array('Nombre' => '2 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/06/19', 'FechaFin' => "2021/09/12", 'NumeroAdultos' => 2, 'NumeroNinos' => 0, 'Temporada' => "Alta", 'PrecioNoche' => "79", 'Estado' => 1),
            array('Nombre' => '2 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/06/19', 'FechaFin' => "2021/09/12", 'NumeroAdultos' => 2, 'NumeroNinos' => 1, 'Temporada' => "Alta", 'PrecioNoche' => "84", 'Estado' => 1),
            array('Nombre' => '2 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/06/19', 'FechaFin' => "2021/09/12", 'NumeroAdultos' => 2, 'NumeroNinos' => 2, 'Temporada' => "Alta", 'PrecioNoche' => "89", 'Estado' => 1),
            array('Nombre' => '2 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/06/19', 'FechaFin' => "2021/09/12", 'NumeroAdultos' => 2, 'NumeroNinos' => 3, 'Temporada' => "Alta", 'PrecioNoche' => "94", 'Estado' => 1),
            array('Nombre' => '2 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/06/19', 'FechaFin' => "2021/09/12", 'NumeroAdultos' => 2, 'NumeroNinos' => 4, 'Temporada' => "Alta", 'PrecioNoche' => "99", 'Estado' => 1),


            // -- 20 de Diciembre al 31 de Diciembre = Temporada Alta

            array('Nombre' => '2 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/12/20', 'FechaFin' => "2021/12/31", 'NumeroAdultos' => 2, 'NumeroNinos' => 0, 'Temporada' => "Alta", 'PrecioNoche' => "55", 'Estado' => 1),
            array('Nombre' => '2 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/12/20', 'FechaFin' => "2021/12/31", 'NumeroAdultos' => 2, 'NumeroNinos' => 1, 'Temporada' => "Alta", 'PrecioNoche' => "60", 'Estado' => 1),
            array('Nombre' => '2 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/12/20', 'FechaFin' => "2021/12/31", 'NumeroAdultos' => 2, 'NumeroNinos' => 2, 'Temporada' => "Alta", 'PrecioNoche' => "65", 'Estado' => 1),
            array('Nombre' => '2 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/12/20', 'FechaFin' => "2021/12/31", 'NumeroAdultos' => 2, 'NumeroNinos' => 3, 'Temporada' => "Alta", 'PrecioNoche' => "70", 'Estado' => 1),
            array('Nombre' => '2 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/12/20', 'FechaFin' => "2021/12/31", 'NumeroAdultos' => 2, 'NumeroNinos' => 4, 'Temporada' => "Alta", 'PrecioNoche' => "75", 'Estado' => 1),
            
            array('Nombre' => '2 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/12/20', 'FechaFin' => "2021/12/31", 'NumeroAdultos' => 2, 'NumeroNinos' => 0, 'Temporada' => "Alta", 'PrecioNoche' => "63", 'Estado' => 1),
            array('Nombre' => '2 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/12/20', 'FechaFin' => "2021/12/31", 'NumeroAdultos' => 2, 'NumeroNinos' => 1, 'Temporada' => "Alta", 'PrecioNoche' => "68", 'Estado' => 1),
            array('Nombre' => '2 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/12/20', 'FechaFin' => "2021/12/31", 'NumeroAdultos' => 2, 'NumeroNinos' => 2, 'Temporada' => "Alta", 'PrecioNoche' => "73", 'Estado' => 1),
            array('Nombre' => '2 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/12/20', 'FechaFin' => "2021/12/31", 'NumeroAdultos' => 2, 'NumeroNinos' => 3, 'Temporada' => "Alta", 'PrecioNoche' => "78", 'Estado' => 1),
            array('Nombre' => '2 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/12/20', 'FechaFin' => "2021/12/31", 'NumeroAdultos' => 2, 'NumeroNinos' => 4, 'Temporada' => "Alta", 'PrecioNoche' => "83", 'Estado' => 1),
            
            array('Nombre' => '2 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/12/20', 'FechaFin' => "2021/12/31", 'NumeroAdultos' => 2, 'NumeroNinos' => 0, 'Temporada' => "Alta", 'PrecioNoche' => "79", 'Estado' => 1),
            array('Nombre' => '2 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/12/20', 'FechaFin' => "2021/12/31", 'NumeroAdultos' => 2, 'NumeroNinos' => 1, 'Temporada' => "Alta", 'PrecioNoche' => "84", 'Estado' => 1),
            array('Nombre' => '2 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/12/20', 'FechaFin' => "2021/12/31", 'NumeroAdultos' => 2, 'NumeroNinos' => 2, 'Temporada' => "Alta", 'PrecioNoche' => "89", 'Estado' => 1),
            array('Nombre' => '2 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/12/20', 'FechaFin' => "2021/12/31", 'NumeroAdultos' => 2, 'NumeroNinos' => 3, 'Temporada' => "Alta", 'PrecioNoche' => "94", 'Estado' => 1),
            array('Nombre' => '2 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/12/20', 'FechaFin' => "2021/12/31", 'NumeroAdultos' => 2, 'NumeroNinos' => 4, 'Temporada' => "Alta", 'PrecioNoche' => "99", 'Estado' => 1),

            
            // -- 01 de Enero al 05 de Enero = Temporada Alta

            array('Nombre' => '2 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/01/01', 'FechaFin' => "2021/01/05", 'NumeroAdultos' => 2, 'NumeroNinos' => 0, 'Temporada' => "Alta", 'PrecioNoche' => "55", 'Estado' => 1),
            array('Nombre' => '2 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/01/01', 'FechaFin' => "2021/01/05", 'NumeroAdultos' => 2, 'NumeroNinos' => 1, 'Temporada' => "Alta", 'PrecioNoche' => "60", 'Estado' => 1),
            array('Nombre' => '2 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/01/01', 'FechaFin' => "2021/01/05", 'NumeroAdultos' => 2, 'NumeroNinos' => 2, 'Temporada' => "Alta", 'PrecioNoche' => "65", 'Estado' => 1),
            array('Nombre' => '2 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/01/01', 'FechaFin' => "2021/01/05", 'NumeroAdultos' => 2, 'NumeroNinos' => 3, 'Temporada' => "Alta", 'PrecioNoche' => "70", 'Estado' => 1),
            array('Nombre' => '2 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/01/01', 'FechaFin' => "2021/01/05", 'NumeroAdultos' => 2, 'NumeroNinos' => 4, 'Temporada' => "Alta", 'PrecioNoche' => "75", 'Estado' => 1),

            array('Nombre' => '2 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/01/01', 'FechaFin' => "2021/01/05", 'NumeroAdultos' => 2, 'NumeroNinos' => 0, 'Temporada' => "Alta", 'PrecioNoche' => "63", 'Estado' => 1),
            array('Nombre' => '2 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/01/01', 'FechaFin' => "2021/01/05", 'NumeroAdultos' => 2, 'NumeroNinos' => 1, 'Temporada' => "Alta", 'PrecioNoche' => "68", 'Estado' => 1),
            array('Nombre' => '2 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/01/01', 'FechaFin' => "2021/01/05", 'NumeroAdultos' => 2, 'NumeroNinos' => 2, 'Temporada' => "Alta", 'PrecioNoche' => "73", 'Estado' => 1),
            array('Nombre' => '2 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/01/01', 'FechaFin' => "2021/01/05", 'NumeroAdultos' => 2, 'NumeroNinos' => 3, 'Temporada' => "Alta", 'PrecioNoche' => "78", 'Estado' => 1),
            array('Nombre' => '2 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/01/01', 'FechaFin' => "2021/01/05", 'NumeroAdultos' => 2, 'NumeroNinos' => 4, 'Temporada' => "Alta", 'PrecioNoche' => "83", 'Estado' => 1),

            array('Nombre' => '2 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/01/01', 'FechaFin' => "2021/01/05", 'NumeroAdultos' => 2, 'NumeroNinos' => 0, 'Temporada' => "Alta", 'PrecioNoche' => "79", 'Estado' => 1),
            array('Nombre' => '2 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/01/01', 'FechaFin' => "2021/01/05", 'NumeroAdultos' => 2, 'NumeroNinos' => 1, 'Temporada' => "Alta", 'PrecioNoche' => "84", 'Estado' => 1),
            array('Nombre' => '2 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/01/01', 'FechaFin' => "2021/01/05", 'NumeroAdultos' => 2, 'NumeroNinos' => 2, 'Temporada' => "Alta", 'PrecioNoche' => "89", 'Estado' => 1),
            array('Nombre' => '2 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/01/01', 'FechaFin' => "2021/01/05", 'NumeroAdultos' => 2, 'NumeroNinos' => 3, 'Temporada' => "Alta", 'PrecioNoche' => "94", 'Estado' => 1),
            array('Nombre' => '2 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/01/01', 'FechaFin' => "2021/01/05", 'NumeroAdultos' => 2, 'NumeroNinos' => 4, 'Temporada' => "Alta", 'PrecioNoche' => "99", 'Estado' => 1),



            // -- 08 de Abril al 12 de Abril = Temporada Alta

            array('Nombre' => '2 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/04/08', 'FechaFin' => "2021/04/12", 'NumeroAdultos' => 2, 'NumeroNinos' => 0, 'Temporada' => "Alta", 'PrecioNoche' => "55", 'Estado' => 1),
            array('Nombre' => '2 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/04/08', 'FechaFin' => "2021/04/12", 'NumeroAdultos' => 2, 'NumeroNinos' => 1, 'Temporada' => "Alta", 'PrecioNoche' => "60", 'Estado' => 1),
            array('Nombre' => '2 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/04/08', 'FechaFin' => "2021/04/12", 'NumeroAdultos' => 2, 'NumeroNinos' => 2, 'Temporada' => "Alta", 'PrecioNoche' => "65", 'Estado' => 1),
            array('Nombre' => '2 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/04/08', 'FechaFin' => "2021/04/12", 'NumeroAdultos' => 2, 'NumeroNinos' => 3, 'Temporada' => "Alta", 'PrecioNoche' => "70", 'Estado' => 1),
            array('Nombre' => '2 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/04/08', 'FechaFin' => "2021/04/12", 'NumeroAdultos' => 2, 'NumeroNinos' => 4, 'Temporada' => "Alta", 'PrecioNoche' => "75", 'Estado' => 1),

            array('Nombre' => '2 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/04/08', 'FechaFin' => "2021/04/12", 'NumeroAdultos' => 2, 'NumeroNinos' => 0, 'Temporada' => "Alta", 'PrecioNoche' => "63", 'Estado' => 1),
            array('Nombre' => '2 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/04/08', 'FechaFin' => "2021/04/12", 'NumeroAdultos' => 2, 'NumeroNinos' => 1, 'Temporada' => "Alta", 'PrecioNoche' => "68", 'Estado' => 1),
            array('Nombre' => '2 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/04/08', 'FechaFin' => "2021/04/12", 'NumeroAdultos' => 2, 'NumeroNinos' => 2, 'Temporada' => "Alta", 'PrecioNoche' => "73", 'Estado' => 1),
            array('Nombre' => '2 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/04/08', 'FechaFin' => "2021/04/12", 'NumeroAdultos' => 2, 'NumeroNinos' => 3, 'Temporada' => "Alta", 'PrecioNoche' => "78", 'Estado' => 1),
            array('Nombre' => '2 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/04/08', 'FechaFin' => "2021/04/12", 'NumeroAdultos' => 2, 'NumeroNinos' => 4, 'Temporada' => "Alta", 'PrecioNoche' => "83", 'Estado' => 1),
            
            array('Nombre' => '2 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/04/08', 'FechaFin' => "2021/04/12", 'NumeroAdultos' => 2, 'NumeroNinos' => 0, 'Temporada' => "Alta", 'PrecioNoche' => "79", 'Estado' => 1),
            array('Nombre' => '2 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/04/08', 'FechaFin' => "2021/04/12", 'NumeroAdultos' => 2, 'NumeroNinos' => 1, 'Temporada' => "Alta", 'PrecioNoche' => "84", 'Estado' => 1),
            array('Nombre' => '2 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/04/08', 'FechaFin' => "2021/04/12", 'NumeroAdultos' => 2, 'NumeroNinos' => 2, 'Temporada' => "Alta", 'PrecioNoche' => "89", 'Estado' => 1),
            array('Nombre' => '2 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/04/08', 'FechaFin' => "2021/04/12", 'NumeroAdultos' => 2, 'NumeroNinos' => 3, 'Temporada' => "Alta", 'PrecioNoche' => "94", 'Estado' => 1),
            array('Nombre' => '2 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/04/08', 'FechaFin' => "2021/04/12", 'NumeroAdultos' => 2, 'NumeroNinos' => 4, 'Temporada' => "Alta", 'PrecioNoche' => "99", 'Estado' => 1),


            // ============ Tarifas 2 Persona =============


            // ============ Tarifas 3 Persona =============




            // -- 01 de Mayo al 18 de Junio = Temporada Baja

            array('Nombre' => '3 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/05/01', 'FechaFin' => "2021/06/18", 'NumeroAdultos' => 3, 'NumeroNinos' => 0, 'Temporada' => "Baja", 'PrecioNoche' => "46", 'Estado' => 1),
            array('Nombre' => '3 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/05/01', 'FechaFin' => "2021/06/18", 'NumeroAdultos' => 3, 'NumeroNinos' => 1, 'Temporada' => "Baja", 'PrecioNoche' => "51", 'Estado' => 1),
            array('Nombre' => '3 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/05/01', 'FechaFin' => "2021/06/18", 'NumeroAdultos' => 3, 'NumeroNinos' => 2, 'Temporada' => "Baja", 'PrecioNoche' => "56", 'Estado' => 1),
            array('Nombre' => '3 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/05/01', 'FechaFin' => "2021/06/18", 'NumeroAdultos' => 3, 'NumeroNinos' => 3, 'Temporada' => "Baja", 'PrecioNoche' => "61", 'Estado' => 1),
            array('Nombre' => '3 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/05/01', 'FechaFin' => "2021/06/18", 'NumeroAdultos' => 3, 'NumeroNinos' => 4, 'Temporada' => "Baja", 'PrecioNoche' => "66", 'Estado' => 1),

            array('Nombre' => '3 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/05/01', 'FechaFin' => "2021/06/18", 'NumeroAdultos' => 3, 'NumeroNinos' => 0, 'Temporada' => "Baja", 'PrecioNoche' => "58", 'Estado' => 1),
            array('Nombre' => '3 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/05/01', 'FechaFin' => "2021/06/18", 'NumeroAdultos' => 3, 'NumeroNinos' => 1, 'Temporada' => "Baja", 'PrecioNoche' => "63", 'Estado' => 1),
            array('Nombre' => '3 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/05/01', 'FechaFin' => "2021/06/18", 'NumeroAdultos' => 3, 'NumeroNinos' => 2, 'Temporada' => "Baja", 'PrecioNoche' => "68", 'Estado' => 1),
            array('Nombre' => '3 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/05/01', 'FechaFin' => "2021/06/18", 'NumeroAdultos' => 3, 'NumeroNinos' => 3, 'Temporada' => "Baja", 'PrecioNoche' => "73", 'Estado' => 1),
            array('Nombre' => '3 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/05/01', 'FechaFin' => "2021/06/18", 'NumeroAdultos' => 3, 'NumeroNinos' => 4, 'Temporada' => "Baja", 'PrecioNoche' => "78", 'Estado' => 1),


            array('Nombre' => '3 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/05/01', 'FechaFin' => "2021/06/18", 'NumeroAdultos' => 3, 'NumeroNinos' => 0, 'Temporada' => "Baja", 'PrecioNoche' => "82", 'Estado' => 1),
            array('Nombre' => '3 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/05/01', 'FechaFin' => "2021/06/18", 'NumeroAdultos' => 3, 'NumeroNinos' => 1, 'Temporada' => "Baja", 'PrecioNoche' => "87", 'Estado' => 1),
            array('Nombre' => '3 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/05/01', 'FechaFin' => "2021/06/18", 'NumeroAdultos' => 3, 'NumeroNinos' => 2, 'Temporada' => "Baja", 'PrecioNoche' => "92", 'Estado' => 1),
            array('Nombre' => '3 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/05/01', 'FechaFin' => "2021/06/18", 'NumeroAdultos' => 3, 'NumeroNinos' => 3, 'Temporada' => "Baja", 'PrecioNoche' => "97", 'Estado' => 1),
            array('Nombre' => '3 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/05/01', 'FechaFin' => "2021/06/18", 'NumeroAdultos' => 3, 'NumeroNinos' => 4, 'Temporada' => "Baja", 'PrecioNoche' => "102", 'Estado' => 1),



            // -- 13 de Abril al 30 de Abril = Temporada Baja

            array('Nombre' => '3 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/04/13', 'FechaFin' => "2021/04/30", 'NumeroAdultos' => 3, 'NumeroNinos' => 0, 'Temporada' => "Baja", 'PrecioNoche' => "46", 'Estado' => 1),
            array('Nombre' => '3 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/04/13', 'FechaFin' => "2021/04/30", 'NumeroAdultos' => 3, 'NumeroNinos' => 1, 'Temporada' => "Baja", 'PrecioNoche' => "51", 'Estado' => 1),
            array('Nombre' => '3 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/04/13', 'FechaFin' => "2021/04/30", 'NumeroAdultos' => 3, 'NumeroNinos' => 2, 'Temporada' => "Baja", 'PrecioNoche' => "56", 'Estado' => 1),
            array('Nombre' => '3 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/04/13', 'FechaFin' => "2021/04/30", 'NumeroAdultos' => 3, 'NumeroNinos' => 3, 'Temporada' => "Baja", 'PrecioNoche' => "61", 'Estado' => 1),
            array('Nombre' => '3 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/04/13', 'FechaFin' => "2021/04/30", 'NumeroAdultos' => 3, 'NumeroNinos' => 4, 'Temporada' => "Baja", 'PrecioNoche' => "66", 'Estado' => 1),

            array('Nombre' => '3 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/04/13', 'FechaFin' => "2021/04/30", 'NumeroAdultos' => 3, 'NumeroNinos' => 0, 'Temporada' => "Baja", 'PrecioNoche' => "58", 'Estado' => 1),
            array('Nombre' => '3 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/04/13', 'FechaFin' => "2021/04/30", 'NumeroAdultos' => 3, 'NumeroNinos' => 1, 'Temporada' => "Baja", 'PrecioNoche' => "63", 'Estado' => 1),
            array('Nombre' => '3 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/04/13', 'FechaFin' => "2021/04/30", 'NumeroAdultos' => 3, 'NumeroNinos' => 2, 'Temporada' => "Baja", 'PrecioNoche' => "68", 'Estado' => 1),
            array('Nombre' => '3 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/04/13', 'FechaFin' => "2021/04/30", 'NumeroAdultos' => 3, 'NumeroNinos' => 3, 'Temporada' => "Baja", 'PrecioNoche' => "73", 'Estado' => 1),
            array('Nombre' => '3 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/04/13', 'FechaFin' => "2021/04/30", 'NumeroAdultos' => 3, 'NumeroNinos' => 4, 'Temporada' => "Baja", 'PrecioNoche' => "78", 'Estado' => 1),

            array('Nombre' => '3 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/04/13', 'FechaFin' => "2021/04/30", 'NumeroAdultos' => 3, 'NumeroNinos' => 0, 'Temporada' => "Baja", 'PrecioNoche' => "83", 'Estado' => 1),
            array('Nombre' => '3 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/04/13', 'FechaFin' => "2021/04/30", 'NumeroAdultos' => 3, 'NumeroNinos' => 1, 'Temporada' => "Baja", 'PrecioNoche' => "87", 'Estado' => 1),
            array('Nombre' => '3 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/04/13', 'FechaFin' => "2021/04/30", 'NumeroAdultos' => 3, 'NumeroNinos' => 2, 'Temporada' => "Baja", 'PrecioNoche' => "92", 'Estado' => 1),
            array('Nombre' => '3 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/04/13', 'FechaFin' => "2021/04/30", 'NumeroAdultos' => 3, 'NumeroNinos' => 3, 'Temporada' => "Baja", 'PrecioNoche' => "97", 'Estado' => 1),
            array('Nombre' => '3 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/04/13', 'FechaFin' => "2021/04/30", 'NumeroAdultos' => 3, 'NumeroNinos' => 4, 'Temporada' => "Baja", 'PrecioNoche' => "102", 'Estado' => 1),





            // -- 13 de Septiembre al 31 de Octubre = Temporada Media

            array('Nombre' => '3 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/09/13', 'FechaFin' => "2021/10/31", 'NumeroAdultos' => 3, 'NumeroNinos' => 0, 'Temporada' => "Media", 'PrecioNoche' => "50", 'Estado' => 1),
            array('Nombre' => '3 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/09/13', 'FechaFin' => "2021/10/31", 'NumeroAdultos' => 3, 'NumeroNinos' => 1, 'Temporada' => "Media", 'PrecioNoche' => "55", 'Estado' => 1),
            array('Nombre' => '3 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/09/13', 'FechaFin' => "2021/10/31", 'NumeroAdultos' => 3, 'NumeroNinos' => 2, 'Temporada' => "Media", 'PrecioNoche' => "60", 'Estado' => 1),
            array('Nombre' => '3 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/09/13', 'FechaFin' => "2021/10/31", 'NumeroAdultos' => 3, 'NumeroNinos' => 3, 'Temporada' => "Media", 'PrecioNoche' => "65", 'Estado' => 1),
            array('Nombre' => '3 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/09/13', 'FechaFin' => "2021/10/31", 'NumeroAdultos' => 3, 'NumeroNinos' => 4, 'Temporada' => "Media", 'PrecioNoche' => "70", 'Estado' => 1),
            
            array('Nombre' => '3 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/09/13', 'FechaFin' => "2021/10/31", 'NumeroAdultos' => 3, 'NumeroNinos' => 0, 'Temporada' => "Media", 'PrecioNoche' => "62", 'Estado' => 1),
            array('Nombre' => '3 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/09/13', 'FechaFin' => "2021/10/31", 'NumeroAdultos' => 3, 'NumeroNinos' => 1, 'Temporada' => "Media", 'PrecioNoche' => "67", 'Estado' => 1),
            array('Nombre' => '3 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/09/13', 'FechaFin' => "2021/10/31", 'NumeroAdultos' => 3, 'NumeroNinos' => 2, 'Temporada' => "Media", 'PrecioNoche' => "72", 'Estado' => 1),
            array('Nombre' => '3 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/09/13', 'FechaFin' => "2021/10/31", 'NumeroAdultos' => 3, 'NumeroNinos' => 3, 'Temporada' => "Media", 'PrecioNoche' => "77", 'Estado' => 1),
            array('Nombre' => '3 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/09/13', 'FechaFin' => "2021/10/31", 'NumeroAdultos' => 3, 'NumeroNinos' => 4, 'Temporada' => "Media", 'PrecioNoche' => "82", 'Estado' => 1),
            
            
            array('Nombre' => '3 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/09/13', 'FechaFin' => "2021/10/31", 'NumeroAdultos' => 3, 'NumeroNinos' => 0, 'Temporada' => "Media", 'PrecioNoche' => "86", 'Estado' => 1),
            array('Nombre' => '3 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/09/13', 'FechaFin' => "2021/10/31", 'NumeroAdultos' => 3, 'NumeroNinos' => 1, 'Temporada' => "Media", 'PrecioNoche' => "91", 'Estado' => 1),
            array('Nombre' => '3 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/09/13', 'FechaFin' => "2021/10/31", 'NumeroAdultos' => 3, 'NumeroNinos' => 2, 'Temporada' => "Media", 'PrecioNoche' => "96", 'Estado' => 1),
            array('Nombre' => '3 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/09/13', 'FechaFin' => "2021/10/31", 'NumeroAdultos' => 3, 'NumeroNinos' => 3, 'Temporada' => "Media", 'PrecioNoche' => "101", 'Estado' => 1),
            array('Nombre' => '3 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/09/13', 'FechaFin' => "2021/10/31", 'NumeroAdultos' => 3, 'NumeroNinos' => 4, 'Temporada' => "Media", 'PrecioNoche' => "106", 'Estado' => 1),

            // -- 01 de Noviembre al 19 de Diciembre = Temporada Media

            array('Nombre' => '3 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/11/01', 'FechaFin' => "2021/12/19", 'NumeroAdultos' => 3, 'NumeroNinos' => 0, 'Temporada' => "Media", 'PrecioNoche' => "50", 'Estado' => 1),
            array('Nombre' => '3 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/11/01', 'FechaFin' => "2021/12/19", 'NumeroAdultos' => 3, 'NumeroNinos' => 1, 'Temporada' => "Media", 'PrecioNoche' => "55", 'Estado' => 1),
            array('Nombre' => '3 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/11/01', 'FechaFin' => "2021/12/19", 'NumeroAdultos' => 3, 'NumeroNinos' => 2, 'Temporada' => "Media", 'PrecioNoche' => "60", 'Estado' => 1),
            array('Nombre' => '3 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/11/01', 'FechaFin' => "2021/12/19", 'NumeroAdultos' => 3, 'NumeroNinos' => 3, 'Temporada' => "Media", 'PrecioNoche' => "65", 'Estado' => 1),
            array('Nombre' => '3 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/11/01', 'FechaFin' => "2021/12/19", 'NumeroAdultos' => 3, 'NumeroNinos' => 4, 'Temporada' => "Media", 'PrecioNoche' => "70", 'Estado' => 1),
            
            array('Nombre' => '3 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/11/01', 'FechaFin' => "2021/12/19", 'NumeroAdultos' => 3, 'NumeroNinos' => 0, 'Temporada' => "Media", 'PrecioNoche' => "62", 'Estado' => 1),
            array('Nombre' => '3 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/11/01', 'FechaFin' => "2021/12/19", 'NumeroAdultos' => 3, 'NumeroNinos' => 1, 'Temporada' => "Media", 'PrecioNoche' => "67", 'Estado' => 1),
            array('Nombre' => '3 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/11/01', 'FechaFin' => "2021/12/19", 'NumeroAdultos' => 3, 'NumeroNinos' => 2, 'Temporada' => "Media", 'PrecioNoche' => "72", 'Estado' => 1),
            array('Nombre' => '3 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/11/01', 'FechaFin' => "2021/12/19", 'NumeroAdultos' => 3, 'NumeroNinos' => 3, 'Temporada' => "Media", 'PrecioNoche' => "77", 'Estado' => 1),
            array('Nombre' => '3 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/11/01', 'FechaFin' => "2021/12/19", 'NumeroAdultos' => 3, 'NumeroNinos' => 4, 'Temporada' => "Media", 'PrecioNoche' => "82", 'Estado' => 1),
            
            array('Nombre' => '3 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/11/01', 'FechaFin' => "2021/12/19", 'NumeroAdultos' => 3, 'NumeroNinos' => 0, 'Temporada' => "Media", 'PrecioNoche' => "86", 'Estado' => 1),
            array('Nombre' => '3 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/11/01', 'FechaFin' => "2021/12/19", 'NumeroAdultos' => 3, 'NumeroNinos' => 1, 'Temporada' => "Media", 'PrecioNoche' => "91", 'Estado' => 1),
            array('Nombre' => '3 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/11/01', 'FechaFin' => "2021/12/19", 'NumeroAdultos' => 3, 'NumeroNinos' => 2, 'Temporada' => "Media", 'PrecioNoche' => "96", 'Estado' => 1),
            array('Nombre' => '3 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/11/01', 'FechaFin' => "2021/12/19", 'NumeroAdultos' => 3, 'NumeroNinos' => 3, 'Temporada' => "Media", 'PrecioNoche' => "101", 'Estado' => 1),
            array('Nombre' => '3 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/11/01', 'FechaFin' => "2021/12/19", 'NumeroAdultos' => 3, 'NumeroNinos' => 4, 'Temporada' => "Media", 'PrecioNoche' => "106", 'Estado' => 1),


            // -- 06 de Enero al 07 de Abril = Temporada Media

            array('Nombre' => '3 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/01/06', 'FechaFin' => "2021/04/07", 'NumeroAdultos' => 3, 'NumeroNinos' => 0, 'Temporada' => "Media", 'PrecioNoche' => "50", 'Estado' => 1),
            array('Nombre' => '3 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/01/06', 'FechaFin' => "2021/04/07", 'NumeroAdultos' => 3, 'NumeroNinos' => 1, 'Temporada' => "Media", 'PrecioNoche' => "55", 'Estado' => 1),
            array('Nombre' => '3 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/01/06', 'FechaFin' => "2021/04/07", 'NumeroAdultos' => 3, 'NumeroNinos' => 2, 'Temporada' => "Media", 'PrecioNoche' => "60", 'Estado' => 1),
            array('Nombre' => '3 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/01/06', 'FechaFin' => "2021/04/07", 'NumeroAdultos' => 3, 'NumeroNinos' => 3, 'Temporada' => "Media", 'PrecioNoche' => "65", 'Estado' => 1),
            array('Nombre' => '3 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/01/06', 'FechaFin' => "2021/04/07", 'NumeroAdultos' => 3, 'NumeroNinos' => 4, 'Temporada' => "Media", 'PrecioNoche' => "70", 'Estado' => 1),

            array('Nombre' => '3 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/01/06', 'FechaFin' => "2021/04/07", 'NumeroAdultos' => 3, 'NumeroNinos' => 0, 'Temporada' => "Media", 'PrecioNoche' => "62", 'Estado' => 1),
            array('Nombre' => '3 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/01/06', 'FechaFin' => "2021/04/07", 'NumeroAdultos' => 3, 'NumeroNinos' => 1, 'Temporada' => "Media", 'PrecioNoche' => "67", 'Estado' => 1),
            array('Nombre' => '3 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/01/06', 'FechaFin' => "2021/04/07", 'NumeroAdultos' => 3, 'NumeroNinos' => 2, 'Temporada' => "Media", 'PrecioNoche' => "72", 'Estado' => 1),
            array('Nombre' => '3 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/01/06', 'FechaFin' => "2021/04/07", 'NumeroAdultos' => 3, 'NumeroNinos' => 3, 'Temporada' => "Media", 'PrecioNoche' => "77", 'Estado' => 1),
            array('Nombre' => '3 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/01/06', 'FechaFin' => "2021/04/07", 'NumeroAdultos' => 3, 'NumeroNinos' => 4, 'Temporada' => "Media", 'PrecioNoche' => "82", 'Estado' => 1),

            array('Nombre' => '3 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/01/06', 'FechaFin' => "2021/04/07", 'NumeroAdultos' => 3, 'NumeroNinos' => 0, 'Temporada' => "Media", 'PrecioNoche' => "86", 'Estado' => 1),
            array('Nombre' => '3 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/01/06', 'FechaFin' => "2021/04/07", 'NumeroAdultos' => 3, 'NumeroNinos' => 1, 'Temporada' => "Media", 'PrecioNoche' => "91", 'Estado' => 1),
            array('Nombre' => '3 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/01/06', 'FechaFin' => "2021/04/07", 'NumeroAdultos' => 3, 'NumeroNinos' => 2, 'Temporada' => "Media", 'PrecioNoche' => "96", 'Estado' => 1),
            array('Nombre' => '3 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/01/06', 'FechaFin' => "2021/04/07", 'NumeroAdultos' => 3, 'NumeroNinos' => 3, 'Temporada' => "Media", 'PrecioNoche' => "101", 'Estado' => 1),
            array('Nombre' => '3 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/01/06', 'FechaFin' => "2021/04/07", 'NumeroAdultos' => 3, 'NumeroNinos' => 4, 'Temporada' => "Media", 'PrecioNoche' => "106", 'Estado' => 1),






            // -- 19 de Junio al 12 de Septiembre = Temporada Alta

            array('Nombre' => '3 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/06/19', 'FechaFin' => "2021/09/12", 'NumeroAdultos' => 3, 'NumeroNinos' => 0, 'Temporada' => "Alta", 'PrecioNoche' => "60", 'Estado' => 1),
            array('Nombre' => '3 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/06/19', 'FechaFin' => "2021/09/12", 'NumeroAdultos' => 3, 'NumeroNinos' => 1, 'Temporada' => "Alta", 'PrecioNoche' => "65", 'Estado' => 1),
            array('Nombre' => '3 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/06/19', 'FechaFin' => "2021/09/12", 'NumeroAdultos' => 3, 'NumeroNinos' => 2, 'Temporada' => "Alta", 'PrecioNoche' => "70", 'Estado' => 1),
            array('Nombre' => '3 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/06/19', 'FechaFin' => "2021/09/12", 'NumeroAdultos' => 3, 'NumeroNinos' => 3, 'Temporada' => "Alta", 'PrecioNoche' => "75", 'Estado' => 1),
            array('Nombre' => '3 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/06/19', 'FechaFin' => "2021/09/12", 'NumeroAdultos' => 3, 'NumeroNinos' => 4, 'Temporada' => "Alta", 'PrecioNoche' => "80", 'Estado' => 1),

            array('Nombre' => '3 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/06/19', 'FechaFin' => "2021/09/12", 'NumeroAdultos' => 3, 'NumeroNinos' => 0, 'Temporada' => "Alta", 'PrecioNoche' => "72", 'Estado' => 1),
            array('Nombre' => '3 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/06/19', 'FechaFin' => "2021/09/12", 'NumeroAdultos' => 3, 'NumeroNinos' => 1, 'Temporada' => "Alta", 'PrecioNoche' => "77", 'Estado' => 1),
            array('Nombre' => '3 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/06/19', 'FechaFin' => "2021/09/12", 'NumeroAdultos' => 3, 'NumeroNinos' => 2, 'Temporada' => "Alta", 'PrecioNoche' => "82", 'Estado' => 1),
            array('Nombre' => '3 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/06/19', 'FechaFin' => "2021/09/12", 'NumeroAdultos' => 3, 'NumeroNinos' => 3, 'Temporada' => "Alta", 'PrecioNoche' => "87", 'Estado' => 1),
            array('Nombre' => '3 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/06/19', 'FechaFin' => "2021/09/12", 'NumeroAdultos' => 3, 'NumeroNinos' => 4, 'Temporada' => "Alta", 'PrecioNoche' => "92", 'Estado' => 1),

            array('Nombre' => '3 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/06/19', 'FechaFin' => "2021/09/12", 'NumeroAdultos' => 3, 'NumeroNinos' => 0, 'Temporada' => "Alta", 'PrecioNoche' => "96", 'Estado' => 1),
            array('Nombre' => '3 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/06/19', 'FechaFin' => "2021/09/12", 'NumeroAdultos' => 3, 'NumeroNinos' => 1, 'Temporada' => "Alta", 'PrecioNoche' => "101", 'Estado' => 1),
            array('Nombre' => '3 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/06/19', 'FechaFin' => "2021/09/12", 'NumeroAdultos' => 3, 'NumeroNinos' => 2, 'Temporada' => "Alta", 'PrecioNoche' => "106", 'Estado' => 1),
            array('Nombre' => '3 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/06/19', 'FechaFin' => "2021/09/12", 'NumeroAdultos' => 3, 'NumeroNinos' => 3, 'Temporada' => "Alta", 'PrecioNoche' => "111", 'Estado' => 1),
            array('Nombre' => '3 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/06/19', 'FechaFin' => "2021/09/12", 'NumeroAdultos' => 3, 'NumeroNinos' => 4, 'Temporada' => "Alta", 'PrecioNoche' => "116", 'Estado' => 1),


            // -- 20 de Diciembre al 31 de Diciembre = Temporada Alta

            array('Nombre' => '3 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/12/20', 'FechaFin' => "2021/12/31", 'NumeroAdultos' => 3, 'NumeroNinos' => 0, 'Temporada' => "Alta", 'PrecioNoche' => "60", 'Estado' => 1),
            array('Nombre' => '3 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/12/20', 'FechaFin' => "2021/12/31", 'NumeroAdultos' => 3, 'NumeroNinos' => 1, 'Temporada' => "Alta", 'PrecioNoche' => "65", 'Estado' => 1),
            array('Nombre' => '3 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/12/20', 'FechaFin' => "2021/12/31", 'NumeroAdultos' => 3, 'NumeroNinos' => 2, 'Temporada' => "Alta", 'PrecioNoche' => "70", 'Estado' => 1),
            array('Nombre' => '3 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/12/20', 'FechaFin' => "2021/12/31", 'NumeroAdultos' => 3, 'NumeroNinos' => 3, 'Temporada' => "Alta", 'PrecioNoche' => "75", 'Estado' => 1),
            array('Nombre' => '3 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/12/20', 'FechaFin' => "2021/12/31", 'NumeroAdultos' => 3, 'NumeroNinos' => 4, 'Temporada' => "Alta", 'PrecioNoche' => "80", 'Estado' => 1),
            
            array('Nombre' => '3 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/12/20', 'FechaFin' => "2021/12/31", 'NumeroAdultos' => 3, 'NumeroNinos' => 0, 'Temporada' => "Alta", 'PrecioNoche' => "72", 'Estado' => 1),
            array('Nombre' => '3 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/12/20', 'FechaFin' => "2021/12/31", 'NumeroAdultos' => 3, 'NumeroNinos' => 1, 'Temporada' => "Alta", 'PrecioNoche' => "77", 'Estado' => 1),
            array('Nombre' => '3 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/12/20', 'FechaFin' => "2021/12/31", 'NumeroAdultos' => 3, 'NumeroNinos' => 2, 'Temporada' => "Alta", 'PrecioNoche' => "82", 'Estado' => 1),
            array('Nombre' => '3 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/12/20', 'FechaFin' => "2021/12/31", 'NumeroAdultos' => 3, 'NumeroNinos' => 3, 'Temporada' => "Alta", 'PrecioNoche' => "87", 'Estado' => 1),
            array('Nombre' => '3 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/12/20', 'FechaFin' => "2021/12/31", 'NumeroAdultos' => 3, 'NumeroNinos' => 4, 'Temporada' => "Alta", 'PrecioNoche' => "92", 'Estado' => 1),
            
            array('Nombre' => '3 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/12/20', 'FechaFin' => "2021/12/31", 'NumeroAdultos' => 3, 'NumeroNinos' => 0, 'Temporada' => "Alta", 'PrecioNoche' => "96", 'Estado' => 1),
            array('Nombre' => '3 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/12/20', 'FechaFin' => "2021/12/31", 'NumeroAdultos' => 3, 'NumeroNinos' => 1, 'Temporada' => "Alta", 'PrecioNoche' => "101", 'Estado' => 1),
            array('Nombre' => '3 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/12/20', 'FechaFin' => "2021/12/31", 'NumeroAdultos' => 3, 'NumeroNinos' => 2, 'Temporada' => "Alta", 'PrecioNoche' => "106", 'Estado' => 1),
            array('Nombre' => '3 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/12/20', 'FechaFin' => "2021/12/31", 'NumeroAdultos' => 3, 'NumeroNinos' => 3, 'Temporada' => "Alta", 'PrecioNoche' => "111", 'Estado' => 1),
            array('Nombre' => '3 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/12/20', 'FechaFin' => "2021/12/31", 'NumeroAdultos' => 3, 'NumeroNinos' => 4, 'Temporada' => "Alta", 'PrecioNoche' => "116", 'Estado' => 1),

            
            // -- 01 de Enero al 05 de Enero = Temporada Alta

            array('Nombre' => '3 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/01/01', 'FechaFin' => "2021/01/05", 'NumeroAdultos' => 3, 'NumeroNinos' => 0, 'Temporada' => "Alta", 'PrecioNoche' => "60", 'Estado' => 1),
            array('Nombre' => '3 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/01/01', 'FechaFin' => "2021/01/05", 'NumeroAdultos' => 3, 'NumeroNinos' => 1, 'Temporada' => "Alta", 'PrecioNoche' => "65", 'Estado' => 1),
            array('Nombre' => '3 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/01/01', 'FechaFin' => "2021/01/05", 'NumeroAdultos' => 3, 'NumeroNinos' => 2, 'Temporada' => "Alta", 'PrecioNoche' => "70", 'Estado' => 1),
            array('Nombre' => '3 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/01/01', 'FechaFin' => "2021/01/05", 'NumeroAdultos' => 3, 'NumeroNinos' => 3, 'Temporada' => "Alta", 'PrecioNoche' => "75", 'Estado' => 1),
            array('Nombre' => '3 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/01/01', 'FechaFin' => "2021/01/05", 'NumeroAdultos' => 3, 'NumeroNinos' => 4, 'Temporada' => "Alta", 'PrecioNoche' => "80", 'Estado' => 1),

            array('Nombre' => '3 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/01/01', 'FechaFin' => "2021/01/05", 'NumeroAdultos' => 3, 'NumeroNinos' => 0, 'Temporada' => "Alta", 'PrecioNoche' => "72", 'Estado' => 1),
            array('Nombre' => '3 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/01/01', 'FechaFin' => "2021/01/05", 'NumeroAdultos' => 3, 'NumeroNinos' => 1, 'Temporada' => "Alta", 'PrecioNoche' => "77", 'Estado' => 1),
            array('Nombre' => '3 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/01/01', 'FechaFin' => "2021/01/05", 'NumeroAdultos' => 3, 'NumeroNinos' => 2, 'Temporada' => "Alta", 'PrecioNoche' => "82", 'Estado' => 1),
            array('Nombre' => '3 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/01/01', 'FechaFin' => "2021/01/05", 'NumeroAdultos' => 3, 'NumeroNinos' => 3, 'Temporada' => "Alta", 'PrecioNoche' => "87", 'Estado' => 1),
            array('Nombre' => '3 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/01/01', 'FechaFin' => "2021/01/05", 'NumeroAdultos' => 3, 'NumeroNinos' => 4, 'Temporada' => "Alta", 'PrecioNoche' => "92", 'Estado' => 1),

            array('Nombre' => '3 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/01/01', 'FechaFin' => "2021/01/05", 'NumeroAdultos' => 3, 'NumeroNinos' => 0, 'Temporada' => "Alta", 'PrecioNoche' => "96", 'Estado' => 1),
            array('Nombre' => '3 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/01/01', 'FechaFin' => "2021/01/05", 'NumeroAdultos' => 3, 'NumeroNinos' => 1, 'Temporada' => "Alta", 'PrecioNoche' => "101", 'Estado' => 1),
            array('Nombre' => '3 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/01/01', 'FechaFin' => "2021/01/05", 'NumeroAdultos' => 3, 'NumeroNinos' => 2, 'Temporada' => "Alta", 'PrecioNoche' => "106", 'Estado' => 1),
            array('Nombre' => '3 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/01/01', 'FechaFin' => "2021/01/05", 'NumeroAdultos' => 3, 'NumeroNinos' => 3, 'Temporada' => "Alta", 'PrecioNoche' => "111", 'Estado' => 1),
            array('Nombre' => '3 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/01/01', 'FechaFin' => "2021/01/05", 'NumeroAdultos' => 3, 'NumeroNinos' => 4, 'Temporada' => "Alta", 'PrecioNoche' => "116", 'Estado' => 1),



            // -- 08 de Abril al 12 de Abril = Temporada Alta

            array('Nombre' => '3 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/04/08', 'FechaFin' => "2021/04/12", 'NumeroAdultos' => 3, 'NumeroNinos' => 0, 'Temporada' => "Alta", 'PrecioNoche' => "60", 'Estado' => 1),
            array('Nombre' => '3 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/04/08', 'FechaFin' => "2021/04/12", 'NumeroAdultos' => 3, 'NumeroNinos' => 1, 'Temporada' => "Alta", 'PrecioNoche' => "65", 'Estado' => 1),
            array('Nombre' => '3 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/04/08', 'FechaFin' => "2021/04/12", 'NumeroAdultos' => 3, 'NumeroNinos' => 2, 'Temporada' => "Alta", 'PrecioNoche' => "70", 'Estado' => 1),
            array('Nombre' => '3 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/04/08', 'FechaFin' => "2021/04/12", 'NumeroAdultos' => 3, 'NumeroNinos' => 3, 'Temporada' => "Alta", 'PrecioNoche' => "75", 'Estado' => 1),
            array('Nombre' => '3 Persona - Solo Alojamiento', 'TipoRegimen' => '1', 'FechaInicio' => '2021/04/08', 'FechaFin' => "2021/04/12", 'NumeroAdultos' => 3, 'NumeroNinos' => 4, 'Temporada' => "Alta", 'PrecioNoche' => "80", 'Estado' => 1),

            array('Nombre' => '3 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/04/08', 'FechaFin' => "2021/04/12", 'NumeroAdultos' => 3, 'NumeroNinos' => 0, 'Temporada' => "Alta", 'PrecioNoche' => "72", 'Estado' => 1),
            array('Nombre' => '3 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/04/08', 'FechaFin' => "2021/04/12", 'NumeroAdultos' => 3, 'NumeroNinos' => 1, 'Temporada' => "Alta", 'PrecioNoche' => "77", 'Estado' => 1),
            array('Nombre' => '3 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/04/08', 'FechaFin' => "2021/04/12", 'NumeroAdultos' => 3, 'NumeroNinos' => 2, 'Temporada' => "Alta", 'PrecioNoche' => "82", 'Estado' => 1),
            array('Nombre' => '3 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/04/08', 'FechaFin' => "2021/04/12", 'NumeroAdultos' => 3, 'NumeroNinos' => 3, 'Temporada' => "Alta", 'PrecioNoche' => "87", 'Estado' => 1),
            array('Nombre' => '3 Persona - Con Desayuno', 'TipoRegimen' => '2', 'FechaInicio' => '2021/04/08', 'FechaFin' => "2021/04/12", 'NumeroAdultos' => 3, 'NumeroNinos' => 4, 'Temporada' => "Alta", 'PrecioNoche' => "92", 'Estado' => 1),
            
            array('Nombre' => '3 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/04/08', 'FechaFin' => "2021/04/12", 'NumeroAdultos' => 3, 'NumeroNinos' => 0, 'Temporada' => "Alta", 'PrecioNoche' => "96", 'Estado' => 1),
            array('Nombre' => '3 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/04/08', 'FechaFin' => "2021/04/12", 'NumeroAdultos' => 3, 'NumeroNinos' => 1, 'Temporada' => "Alta", 'PrecioNoche' => "101", 'Estado' => 1),
            array('Nombre' => '3 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/04/08', 'FechaFin' => "2021/04/12", 'NumeroAdultos' => 3, 'NumeroNinos' => 2, 'Temporada' => "Alta", 'PrecioNoche' => "106", 'Estado' => 1),
            array('Nombre' => '3 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/04/08', 'FechaFin' => "2021/04/12", 'NumeroAdultos' => 3, 'NumeroNinos' => 3, 'Temporada' => "Alta", 'PrecioNoche' => "111", 'Estado' => 1),
            array('Nombre' => '3 Persona - Media Pension', 'TipoRegimen' => '3', 'FechaInicio' => '2021/04/08', 'FechaFin' => "2021/04/12", 'NumeroAdultos' => 3, 'NumeroNinos' => 4, 'Temporada' => "Alta", 'PrecioNoche' => "116", 'Estado' => 1),


            // ============ Tarifas 3 Persona =============

        );



        DB::table('tarifas')->insert($tarifas);
    }
}
