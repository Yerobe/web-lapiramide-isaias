<h1>Hi, {{ $name }} here you have all the details of your reservation</h1>


<h4>Reservation number: {{$code}}</h4>
<h4>Name and surname: {{$name}} {{$surname}}</h4>
<h4>Number of nights: {{$night}}</h4>
<h4>Total price: {{$price}}€</h4>
<h4>Check-in date: {{$checkin}}</h4>
<h4>Check-out date: {{$checkout}}</h4>
<h4>Number of adults: {{$adults}}</h4>
<h4>Number of children: {{$children}}</h4>
<h4>Number of Pets: {{$pets}}</h4>

<h1>La Piramide</h1>