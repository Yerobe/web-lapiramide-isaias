@extends('layouts.master')

@section('content')



<a id="button"></a>


<div class="row mt-5 ">
    <div class="col col-xl-3 col-lg-3 col-md-0 col-sm-0 col-0"></div>
    <div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 text-center">
        <a href="/"><img src="{{asset('./HomePage/img/logo.png')}}" alt="Logo La Piramide "></a>
    </div>
    <div class="col col-xl-3 col-lg-3 col-md-0 col-sm-0 col-0"></div>


</div>

<hr>


<div class="row">
    <div class="col col-xl-2 col-lg-2 col-md-1 col-sm-1 col-1"></div>
    <div class="col col-xl-8 col-lg-8 col-md-10 col-sm-10 col-10">
        <div class="rounded mb-5 m-auto shadow">
            <form action="/rooms" method="POST" class="">
                @csrf
                <div style="margin-top: 2%" class="form-row text-white justify-content-around p-4">
                    <div class="form-group col-md-3">
                        <label for="inputEmail4">Arrival Date</label>
                        <input type="date" class="form-control" name="fecha_inicio" id="inputEmail4" value="<?php echo ($arrayRuta["fechaInicio"]); ?>">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="inputPassword4">Departure Date</label>
                        <input type="date" class="form-control" name="fecha_final" id="inputPassword4" value="<?php echo ($arrayRuta["fechaFinal"]); ?>">
                    </div>
                    <div class="form-group col-md-1">
                        <label for="inputEmail4">Adults</label>
                        <input type="number" class="form-control" name="adultos" id="inputEmail4" min="1" max="4" value="<?php echo ($arrayRuta["adultos"]); ?>">
                    </div>
                    <div class="form-group col-md-1">
                        <label for="inputPassword4">Children</label>
                        <input type="number" class="form-control" name="niños" id="inputPassword4" min="0" max="4" value="<?php echo ($arrayRuta["niños"]); ?>">
                    </div>

                    <div class="form-group col-md-1">
                        <label for="inputPassword4">Pets</label>
                        <input type="number" class="form-control" name="mascotas" id="inputPassword4" min="0" max="4" value="<?php echo ($arrayRuta["mascotas"]); ?>">
                    </div>

                    <div class="form-group col-md-3">
                        <button type="submit" class="form-control btn bg-light-blue text-white rounded-0">Change Date</button>
                    </div>


                </div>

            </form>
        </div>
    </div>
    <div class="col col-xl-2 col-lg-2 col-md-1 col-sm-1 col-1"></div>
</div>

<div class="row mt-5">
    <div class="col col-2"></div>
    <div class="col col-8">
        <h4>STEP 1 - ROOMS SELECTION</h4>
        <hr>
    </div>
    <div class="col col-2"></div>
</div>

<!-- SOLO ALOJAMIENTO  -->

<?php
if(count($arraySoloAlojamiento) != 0) {
?>

    <div class="row mt-5">
        <div class="col col-xl-2 col-lg-1 col-md-1 col-sm-1 col-1"></div>
        <div class="col col-xl-8 col-lg-10 col-md-10 col-sm-10 col-10 shadow p-4">
            <div class="row">
                <div class="col col-xl-5 col-lg-5 col-md-12 col-sm-12 col-12 mt-4">
                    <img class="w-100 h-100" src="https://www.demediterraning.com/V05/img/dem/Alojamientos/72339/72339_64625019.jpg" alt="">
                </div>
                <div class="col col-xl-5 col-lg-5 col-md-12 col-sm-12 col-12m mt-4">
                    <h5>{{$arraySoloAlojamiento["Nombre"]}}</h5>
                    <h5 class="small color-light-only-blue">DORSET GRAND SUBANG</h5>

                    <p>Apartment of 38 square meters, with balcony, views of the pool, private bathroom, flat screen TV, etc ...</p>

                    <a href="#" class="text-decoration-none">More Details</a>
                </div>

                <div class="col col-xl-2 col-lg-2 col-md-12 col-sm-12 col-12 text-center my-auto mt-4">
                    <h5 class="">FROM</h5>
                    <h2>{{$arraySoloAlojamiento["PrecioTotal"]}}€</h2>

                    <a href="<?php echo("/room/create"."/".$arraySoloAlojamiento["Id"]."/".$arraySoloAlojamiento["fechaInicio"]."/".$arraySoloAlojamiento["fechaFinal"]."/".$arraySoloAlojamiento["adultos"]."/".$arraySoloAlojamiento["niños"]."/".$arraySoloAlojamiento["mascotas"]."/".$arraySoloAlojamiento["noches"]."/".$arraySoloAlojamiento["PrecioTotal"])?>" class="btn btn-block bg-light-only-blue mt-5 text-white">SELECT THIS</a>
                </div>
            </div>
        </div>
        <div class="col col-xl-2 col-lg-1 col-md-1 col-sm-1 col-1"></div>
    </div>

<?php
}
?>

<!-- DESAYUNOS -->

<?php
if(count($arrayDesayunos) != 0) {
?>

    <div class="row mt-5">
        <div class="col col-xl-2 col-lg-1 col-md-1 col-sm-1 col-1"></div>
        <div class="col col-xl-8 col-lg-10 col-md-10 col-sm-10 col-10 shadow p-4">
            <div class="row">
                <div class="col col-xl-5 col-lg-5 col-md-12 col-sm-12 col-12 mt-4">
                    <img class="w-100 h-100" src="https://www.demediterraning.com/V05/img/dem/Alojamientos/72339/72339_64625019.jpg" alt="">
                </div>
                <div class="col col-xl-5 col-lg-5 col-md-12 col-sm-12 col-12m mt-4">
                    <h5>{{$arrayDesayunos["Nombre"]}}</h5>
                    <h5 class="small color-light-only-blue">DORSET GRAND SUBANG</h5>

                    <p>Apartment of 38 square meters, with balcony, views of the pool, private bathroom, flat screen TV, etc ...</p>

                    <a href="#" class="text-decoration-none">More Details</a>
                </div>

                <div class="col col-xl-2 col-lg-2 col-md-12 col-sm-12 col-12 text-center my-auto mt-4">
                    <h5 class="">FROM</h5>
                    <h2>{{$arrayDesayunos["PrecioTotal"]}}€</h2>

                    <a href="<?php echo("/room/create"."/".$arrayDesayunos["Id"]."/".$arrayDesayunos["fechaInicio"]."/".$arrayDesayunos["fechaFinal"]."/".$arrayDesayunos["adultos"]."/".$arrayDesayunos["niños"]."/".$arrayDesayunos["mascotas"]."/".$arrayDesayunos["noches"]."/".$arrayDesayunos["PrecioTotal"])?>" class="btn btn-block bg-light-only-blue mt-5 text-white">SELECT THIS</a>
                </div>
            </div>
        </div>
        <div class="col col-xl-2 col-lg-1 col-md-1 col-sm-1 col-1"></div>
    </div>

<?php
}
?>

<!-- MEDIA PENSION -->

<?php
if(count($arrayMediaPension) != 0) {
?>

    <div class="row mt-5">
        <div class="col col-xl-2 col-lg-1 col-md-1 col-sm-1 col-1"></div>
        <div class="col col-xl-8 col-lg-10 col-md-10 col-sm-10 col-10 shadow p-4">
            <div class="row">
                <div class="col col-xl-5 col-lg-5 col-md-12 col-sm-12 col-12 mt-4">
                    <img class="w-100 h-100" src="https://www.demediterraning.com/V05/img/dem/Alojamientos/72339/72339_64625019.jpg" alt="">
                </div>
                <div class="col col-xl-5 col-lg-5 col-md-12 col-sm-12 col-12m mt-4">
                    <h5>{{$arrayMediaPension["Nombre"]}}</h5>
                    <h5 class="small color-light-only-blue">DORSET GRAND SUBANG</h5>

                    <p>Apartment of 38 square meters, with balcony, views of the pool, private bathroom, flat screen TV, etc ...</p>

                    <a href="#" class="text-decoration-none">More Details</a>
                </div>

                <div class="col col-xl-2 col-lg-2 col-md-12 col-sm-12 col-12 text-center my-auto mt-4">
                    <h5 class="">FROM</h5>
                    <h2>{{$arrayMediaPension["PrecioTotal"]}}€</h2>

                    <a href="<?php echo("/room/create"."/".$arrayMediaPension["Id"]."/".$arrayMediaPension["fechaInicio"]."/".$arrayMediaPension["fechaFinal"]."/".$arrayMediaPension["adultos"]."/".$arrayMediaPension["niños"]."/".$arrayMediaPension["mascotas"]."/".$arrayMediaPension["noches"]."/".$arrayMediaPension["PrecioTotal"])?>" class="btn btn-block bg-light-only-blue mt-5 text-white">SELECT THIS</a>
                </div>
            </div>
        </div>
        <div class="col col-xl-2 col-lg-1 col-md-1 col-sm-1 col-1"></div>
    </div>

<?php
}
?>



@endsection