<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>LaPiramide</title>

    <style>

    *{
        text-align: center;
        margin: auto;
    }

    img{
        width: 20%;
        margin-top: 2%;
    }

    h1{
        margin-top: 1%;
        font-size: 150px;
    }

    p{
        font-size: 30px;
        margin-top: 20px;
    }

    a{
        color: black;
    }
    </style>
</head>



<body>

<img src="{{asset('errores/404.png')}}" alt="Flaticon: Pixel Perfect">

    <h1>Error 500</h1>
    <p>Woops. Looks Like this server have a problem.</p>
    <p>Try to access more later</p>



</body>

</html>