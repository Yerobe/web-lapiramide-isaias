@extends('layouts.master')

@section('content')
@if(Session::has('Mandado'))

<div class="modal fade" id="ModalEmail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Notice</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                "An email was sent with the details of your reservation"

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>



@endif

@if(Session::has('Realizada'))
<div class="shadow-lg shadow-top bg-white rounded m-3 mt-2 p-2">
    <div data-aos="zoom-in-up" style="margin-top: 150px;" class="row">
        <div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">

            <div class="row">
                <div class="col col-xl-3 col-lg-1 col-md-1 col-sm-1 col-1"></div>
                <div class="col col-xl-9 col-lg-10 col-md-10 col-sm-10 col-10">
                    <p style="color: #48C3B2; ">THE RESERVATION HAS BEEN SUCCESSFULLY MADE</p>
                    <h2>Here you can see your reservation details and download them in PDF</h2>
                    <h4 style="color: #4C4C4C; " class="mt-5 ">Name: {{$arrayDatosMostrar["nombre"]}} {{$arrayDatosMostrar["apellidos"]}}</h4>
                    <h4 style="color: #4C4C4C; " class="mt-5 ">Check-in date: {{$arrayDatosMostrar["fechaInicio"]}} </h4>
                    <h4 style="color: #4C4C4C; " class="mt-5 ">Check-out date: {{$arrayDatosMostrar["fechaFinal"]}} </h4>
                    <h4 style="color: #4C4C4C; " class="mt-5 ">Nights: {{$arrayDatosMostrar["noches"]}} </h4>
                    <h4 style="color: #4C4C4C; " class="mt-5 ">Total Price: {{$arrayDatosMostrar["precio"]}} </h4>
                    <h4 style="color: #4C4C4C; " class="mt-5 mb-3 ">Reservation number: <strong>{{$arrayDatosMostrar["codigo"]}}</strong> </h4>

                </div>
                <div class="col col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1"></div>
            </div>

        </div>
        <div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 m-auto text-center">

            <a href="<?php echo ("/generate-pdf" . "/" . $arrayDatosMostrar["IdReserva"] . "/" . $arrayDatosMostrar["IdCliente"] . "/" . $arrayDatosMostrar["noches"] . "/" . $arrayDatosMostrar["precio"]) ?>"><button style="width: 70%; height: 400px; font-size: 50px;" class="form-control btn bg-light-blue text-white rounded-pill">Descargar PDF</button></a>

        </div>

    </div>
</div>


@endif




<a id="button"></a>

<div class="row mt-5 ">
    <div class="col col-xl-3 col-lg-3 col-md-0 col-sm-0 col-0"></div>
    <div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 text-center">
        <img src="{{asset('./HomePage/img/logo.png')}}" alt="Logo La Piramide ">
    </div>
    <div class="col col-xl-3 col-lg-3 col-md-0 col-sm-0 col-0"></div>


</div>

<hr>


<div class="row ">
    <div class="col col-12 ">
        <ul class="nav justify-content-center text-danger ">
            <li class="nav-item p-2 ">
                <a class="nav-link text-dark" href="# ">Home</a>
            </li>
            <li class="nav-item p-2 ">
                <a href="#aboutUs" class="nav-link text-dark">About Us</a>
            </li>
            <li class="nav-item p-2 ">
                <a href="#services" class="nav-link text-dark">Services</a>
            </li>

            <li class="nav-item p-2 ">
                <a href="#rooms" class="nav-link text-dark">Rooms</a>
            </li>

            <li class="nav-item p-2 ">
                <a href="#events" class="nav-link text-dark">Events</a>
            </li>

            <li class="nav-item p-2 ">
                <a href="#contactUs" class="nav-link text-dark ">Contact Us</a>
            </li>

        </ul>
    </div>
</div>



<div class="row mt-3 ">
    <div class="col col-12 ">

        <div style="width: 100%;" id="carouselExampleIndicators" class="carousel slide w-101" data-ride="carousel">

            <div class="carousel-inner">
                <div class="carousel-item active w-100">
                    <img class="carousel-principal d-block" src="{{asset('./HomePage/img/LP_Portada_1.JPG')}}" alt="...">
                </div>
                <div class="carousel-item w-100">
                    <img class="carousel-principal d-block" src="{{asset('./HomePage/img/LP_Portada_2.JPG')}}" alt="...">
                </div>
                <div class="carousel-item w-100">
                    <img class="carousel-principal d-block" src="{{asset('./HomePage/img/LP_Portada_3.JPG')}}" alt="...">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon " aria-hidden="true "></span>
                <span class="sr-only ">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only ">Next</span>
            </a>
        </div>



        <form action="/rooms" method="POST" class="container-booking shadow rounded mb-5 m-auto">
            @csrf
            <div style="margin-top: 2% " class="form-row text-white justify-content-around p-4">
                <div class="form-group col-md-3 ">
                    <label for="inputEmail4 ">Arrival Date</label>
                    <input type="date" required name="fecha_inicio" class="form-control" id="fecha_entrada" onchange="checkCommunity()">
                </div>
                <div class="form-group col-md-3 ">
                    <label for="inputPassword4 ">Departure Date</label>
                    <input type="date" required name="fecha_final" class="form-control" id="fecha_salida">
                </div>
                <div class="form-group col-md-1 ">
                    <label for="inputEmail4 ">Adults</label>
                    <input type="number" required name="adultos" class="form-control " id="inputEmail4" min="1" max="4" value="1">
                </div>
                <div class="form-group col-md-1 ">
                    <label for="inputPassword4 ">Children</label>
                    <input type="number" name="niños" class="form-control " id="inputPassword4" min="0" max="4" value="0">
                </div>

                <div class="form-group col-md-1 ">
                    <label for="inputPassword4 ">Pets</label>
                    <input type="number" name="mascotas" class="form-control" id="inputPassword4" min="0" max="4" value="0">
                </div>

                <div class="form-group col-md-3 ">
                    <button type="submit" class="form-control btn bg-light-blue text-white rounded-0">Check Availability</button>
                </div>


            </div>

        </form>




    </div>
</div>



<a name="aboutUs"></a>
<div data-aos="zoom-in-up" style="margin-top: 150px;" class="row">
    <div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">


        <div class="row">
            <div class="col col-xl-3 col-lg-1 col-md-1 col-sm-1 col-1"></div>
            <div class="col col-xl-9 col-lg-10 col-md-10 col-sm-10 col-10">
                <p style="color: #48C3B2; ">ABOUT US</p>
                <h2>Welcome to Apartaments La Piramide Fuerteventura</h2>
                <h4 style="color: #4C4C4C; " class="mt-5 ">With over 200 apartments , Apartamentos La Piramide offers a wide variety catering for a perfect stay no matter where your destination.</h4>
                <p style="color: #48C3B2; " class="mt-5 ">LA PIRAMIDE</p>
            </div>
            <div class="col col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1"></div>
        </div>




    </div>
    <div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 m-auto text-center">


        <img class="m-1 rounded effects-about img-about" style="width: 230px; height: 205px;" src="https://cf.bstatic.com/images/hotel/max1024x768/149/149249662.jpg " alt=" ">
        <img class="m-1 rounded effects-about img-about" style="width: 230px; height: 205px;" src="https://cf.bstatic.com/images/hotel/max1024x768/149/149249662.jpg " alt=" ">




    </div>

</div>



<a name="services"></a>
<div style="margin-left: 23px;" data-aos="zoom-in-up " class="row mt-5 mb-5">
    <div class="col col-12 justify-content-center d-inline-flex flex-wrap">
        <div class="shadow-lg bg-white rounded m-3 p-2" style="width: 190px; height: 150px; ">
            <img class="mt-4 ml-auto mr-auto d-block" style="width: 40px; height: 40px;" src="{{asset('./HomePage/img/icons/wifi_icon.png')}}" alt="Flaticon = Freepik">
            <h4 class="text-center mt-4 ">Free Wifi</h4>
        </div>
        <div class="shadow-lg bg-white rounded m-3 p-2" style="width: 190px; height: 150px; ">
            <img class="mt-4 ml-auto mr-auto d-block" style="width: 40px; height: 40px;" src="{{asset('./HomePage/img/icons/gim_icon.png')}}" alt="Flaticon = Freepik">
            <h4 class="text-center mt-4 ">Gym</h4>
        </div>
        <div class="shadow-lg bg-white rounded m-3 p-2" style="width: 190px; height: 150px; ">
            <img class="mt-4 ml-auto mr-auto d-block" style="width: 40px; height: 40px;" src="{{asset('./HomePage/img/icons/lavanderia_icon.png')}}" alt="Flaticon = Freepik">
            <h4 class="text-center mt-4 ">Laundry</h4>
        </div>
        <div class="shadow-lg bg-white rounded m-3 p-2" style="width: 190px; height: 150px; ">
            <img class="mt-4 ml-auto mr-auto d-block" style="width: 40px; height: 40px;" src="{{asset('./HomePage/img/icons/limpieza_icon.png')}}" alt="Flaticon = Ultimatearm">
            <h4 class="text-center mt-4 ">Cleaning</h4>
        </div>
        <div class="shadow-lg bg-white rounded m-3 p-2" style="width: 190px; height: 150px; ">
            <img class="mt-4 ml-auto mr-auto d-block" style="width: 40px; height: 40px;" src="{{asset('./HomePage/img/icons/ServicioBar_icon.png')}}" alt="Flaticon = Carkuro">
            <h4 class="text-center mt-4 ">Bar Service</h4>
        </div>
    </div>
</div>

<a name="rooms"></a>
<div data-aos="zoom-in-up " class="row bg-contact ">
    <div class="col col-2 ">

    </div>

    <div class="col col-8 text-center mt-5 mb-5 ">
        <h4 class="text-title mb-4 ">Book a Room</h4>
        <p class="text-description ">
        You can check the apartments independently in detail, in this way you will not lose in any subjective way the details of these.
        </p>

        <div class="my-5 text-center container">

            <div class="row d-flex align-items-center">
                <div class="col-1 d-flex align-items-center justify-content-center"></div>
                <div class="col-10">
                    <!--Start carousel-->
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">

                        <div class="carousel-inner ">
                            <div class="carousel-item active">
                                <div class="row">
                                    <div style="background-image:url( 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/46992/flexfun04.jpg');" class="col-12 col-md d-flex align-items-center justify-content-center">
                                    </div>
                                    <div style="background-image:url( 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/46992/flexfun02.jpg');" class="col-12 col-md d-flex align-items-center justify-content-center">
                                    </div>
                                    <div style="background-image:url( 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/46992/flexfun05.jpg');" class="col-12 col-md d-flex align-items-center justify-content-center" class="col-12 col-md d-flex align-items-center
                    justify-content-center ">
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="row">
                                    <div style="background-image:url( 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/46992/flexfun01.jpg'); " class="col-12 col-md d-flex align-items-center justify-content-center">
                                    </div>
                                    <div style="background-image:url( 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/46992/flexfun03.jpg'); " class="col-12 col-md d-flex align-items-center justify-content-center">
                                    </div>
                                    <div style="background-image:url( 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/46992/flexfun06.jpg'); " class="col-12 col-md d-flex align-items-center justify-content-center" class="col-12 col-md d-flex align-items-center
                    justify-content-center">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--End carousel-->
                </div>

                <div class="col-1 d-flex align-items-center justify-content-center ">

                </div>

            </div>
        </div>


    </div>
</div>


<a name="events"></a>
<div data-aos="zoom-in-up " class="row mt-5 mb-5 ">
    <div class="row ">
        <div class="col col-2 "></div>
        <div class="col col-8 mb-5 text-center ">

            <h4 class="text-title mb-4 ml-5 ">Latest News & Events</h4>
            <p class="text-description ">Stay informed at all times in relation to the events "La Piramide", 
            every week we carry out all kinds of events, such as: Karaoke, Bingo, Games with fire, Typical Dances, etc ...
            </p>
        </div>
        <div class="col col-2 "></div>
    </div>
    <div class="col col-12 d-flex flex-wrap justify-content-around ">

        <?php
        foreach ($arrayEventos as $event) {
        ?>
            <div class="card border-0 ml-2" style="width: 360px; ">
                <img class="card-img-top "  src="{{asset('./eventos/img')}}<?php echo ("/" . $event->ImagenURL); ?>" alt="Card image cap ">
                <div class="card-body ">
                    <h5 style="color: #AFB4BF " class="card-title ">{{$event->Titulo}} - {{$event->Fecha}}</h5>
                    <h5 class="card-title ">{{$event->Titulo}}</h5>
                    <p class="card-text ">{{$event->Descripcion}}</p>
                    <a href="# " class=" ">Lean More -></a>
                </div>
            </div>

        <?php
        }
        ?>
    </div>
</div>






<a name="contactUs"></a>
<div data-aos="zoom-in-up " class="row bg-contact mt-5 ">
    <div class="col col-xl-4 col-lg-4 col-md-1 col-sm-1 col-1 "></div>
    <div class="col col-xl-4 col-lg-4 col-md-10 col-sm-10 col-10 ">

        <h2 class="display-4 text-center mt-4 ">Contact</h2>
        <form class="mt-5 mb-5 ">
            <div class="form-row ">
                <div class="form-group col-md-6 ">
                    <input type="text " class="form-control " id="inputEmail4 " placeholder="Name * ">
                </div>
                <div class="form-group col-md-6 ">
                    <input type="email " class="form-control " id="inputPassword4 " placeholder="Email * ">
                </div>
            </div>
            <div class="form-row ">
                <div class="form-group col-md-6 ">
                    <input type="text " class="form-control " id="inputEmail4 " placeholder="Asunt * ">
                </div>

                <div class="form-group col-md-6 ">
                    <select required class="form-control " name=" " id=" ">
                        <option disabled selected>Choose Topic</option>
                        <option value=" ">Prices</option>
                        <option value=" ">Sugerencias</option>
                        <option value=" ">Novedades</option>
                        <option value=" ">Conciertos</option>
                    </select>
                </div>
            </div>

            <div class="form-row ">
                <div class="form-group col-md-12 ">
                    <textarea style="height: 150px; " type="text " class="form-control input-mensaje " id="inputEmail4 " placeholder="Message * "></textarea>
                </div>
            </div>



            <button type="submit " class="btn btn-block bg-gray-0 text-white ">Send</button>
        </form>
    </div>
    <div class="col col-xl-4 col-lg-4 col-md-1 col-sm-1 col-1"></div>
</div>














<div class="row ">
    <div class="col col-12 d-inline-flex flex-wrap">
        <div class="img-lines bg-img-1">
        </div>

        <div class="img-lines bg-img-2">

        </div>

        <div class="img-lines bg-img-3">

        </div>

        <div class="img-lines bg-img-4">

        </div>



    </div>
</div>

@endsection