<div class="row bg-gray-0 text-white" id="home">
    <div class="col col-xl-1 col-lg-0 col-md-0 col-sm-0 col-0"></div>
    <div class="col col-xl-6 col-lg-7 col-md-9 col-sm-12 col-12 mt-3">

        <div class="row">
            <div class="col col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 text-center">
                <i class="bi-telephone-fill d-inline-block"></i>
                <p class="d-inline-block ml-3">+34 928 858 347</p>
            </div>
            <div class="col col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12 text-center">
                <i class="bi-map-fill d-inline-block"></i>
                <p class="d-inline-block ml-3 ">C/ Berlina 7 - 35610 - Costa de Antigua</p>
            </div>
        </div>



    </div>
    <div class="col col-xl-3 col-lg-3 col-md-1 col-sm-0 col-0 "></div>
    <div class="col col-xl-2 col-lg-2 col-md-2 col-sm-12 col-12 mt-3 mx-auto text-center mb-3">
        <a class="text-decoration-none" href="https://www.facebook.com/hotellapiramide" target="_blank">
            <i class="bi-facebook mr-2 "></i>
        </a>

        <a class="text-decoration-none" href="https://www.instagram.com/apartamentoslapiramide/" target="_blank">
            <i class="bi-instagram mr-2 "></i>
        </a>

        <a class="text-decoration-none" href="#">
            <i class="bi-whatsapp mr-2 "></i>
        </a>


    </div>

</div>