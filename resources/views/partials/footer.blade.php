<div class="row mt-5 bg-footer">
        <div class="col col-12">
            <footer>
                <div class="row justify-content-center p-5">
                    <div class="col col-xl-2 col-lg-6 col-md-12 col-sm-12 col-12 text-light-brown">
                        <h4 class="mb-4 text-white ">La Piramide</h4>
                        <p class="small ">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quaerat eligendi alias a exercitationem eos sed, nam dolorum labore amet illo. Ducimus ullam voluptates cupiditate, culpa inventore repellat laboriosam dicta sit.</p>
                    </div>
                    <div class="col col-xl-2 col-lg-6 col-md-12 col-sm-12 col-12 text-light-brown ">
                        <h5 class="mb-4 small text-white ">CONTACT US</h5>
                        <p class="small ">35610 - Costa de Antigua, Fuerteventura</p>
                        <p class="small ">+34 928 858 347</p>
                        <p class="small ">apartamentoslapiramide@gmail.com</p>
                    </div>
                    <div class="col col-xl-2 col-lg-6 col-md-12 col-sm-12 col-12 text-light-brown ml-5">
                        <h4 class="mb-4 small text-white ">QUICK LINKS</h4>
                        <a href="/" class="enlaceHome text-decoration-none text-secondary"><p class="small ">Home</p></a>
                        <p class="small ">Terms & Conditions</p>
                        <p class="small ">FAQ</p>
                        <p class="small ">Newletter</p>
                    </div>
                    <div class="col col-xl-2 col-lg-6 col-md-12 col-sm-12 col-12 text-light-brown ">
                        <h5 class="mb-4 small text-white ">DON´T MISS ANY UPDATES</h5>

                        <form action=" ">
                            <div class="form-row text-white ">
                                <div class="form-group col-md-10 ">
                                    <input style="height: 50px; " type="email " class="form-control " id="inputEmail4 " placeholder="Your Email ">
                                    <button style="background-color: black; " type="submit " class="btn btn-block mt-4 text-white ">SEND</button>
                                </div>

                            </div>
                        </form>
                    </div>



                </div>
            </footer>
        </div>
    </div>
    <div style="background-color: black; " class="row ">
        <div class="col col-12 ">
            <p class="text-center my-auto p-3 ">© Copyright - Team Isaias, Jerobe, Luis. All rights Reserved.</p>
        </div>
    </div>




    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js "></script>


    <script>
        AOS.init();
    </script>

    <script>
        var btn = $('#button');

        $(window).scroll(function() {
            if ($(window).scrollTop() > 300) {
                btn.addClass('show');
            } else {
                btn.removeClass('show');
            }
        });

        btn.on('click', function(e) {
            e.preventDefault();
            $('html, body').animate({
                scrollTop: 0
            }, '300');
        });
    </script>

    <script src="js/AnimatedScroll/jquery-1.8.3.min.js "></script>
    <script src="js/AnimatedScroll/animatescroll.js "></script>
