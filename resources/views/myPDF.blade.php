@extends('layouts.pdf')

@section('content')

<div style="margin-top: 150px;" class="row">

    <div class="col col-12 text-center">
        <h2>Muchas gracias su reserva esta confirmada</h2>
    </div>


</div>

<div class="row">

    <div class="col col-12 text-center">
        <p style="color: #48C3B2; ">YOUR RESERVATION INFORMATION</p>
        <h4 style="color: #4C4C4C; " class="mt-5 ">Reservation number: {{$NumeroReserva}}</h4>
        <h4 style="color: #4C4C4C; " class="mt-5 ">Name: {{$Nombre}} {{$Apellidos}}</h4>
        <h4 style="color: #4C4C4C; " class="mt-5 ">Nigths: {{$Noches}}</h4>
        <h4 style="color: #4C4C4C; " class="mt-5 ">Total Price: {{$Precio}}</h4>
        <h4 style="color: #4C4C4C; " class="mt-5 ">Check-in: {{$FechaEntrada}}</h4>
        <h4 style="color: #4C4C4C; " class="mt-5 ">Check-out: {{$FechaSalida}}</h4>
        <h4 style="color: #4C4C4C; " class="mt-5 ">Number of adults: {{$NumeroAdultos}}</h4>
        <h4 style="color: #4C4C4C; " class="mt-5 ">Number of children: {{$NumeroNinios}}</h4>
        <h4 style="color: #4C4C4C; " class="mt-5 ">Number of pets: {{$NumeroMascotas}}</h4>
        <p style="color: #48C3B2; " class="mt-5 ">LA PIRAMIDE</p>
    </div>

</div>



@endsection