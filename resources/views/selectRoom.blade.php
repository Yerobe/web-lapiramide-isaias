@extends('layouts.master')

@section('content')


<div class="row mt-5 ">
        <div class="col col-xl-3 col-lg-3 col-md-0 col-sm-0 col-0"></div>
        <div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 text-center">
            <a href="/"><img src="{{asset('./HomePage/img/logo.png')}}" alt="Logo La Piramide "></a>
        </div>
        <div class="col col-xl-3 col-lg-3 col-md-0 col-sm-0 col-0"></div>


    </div>

    <hr>


    <div class="row mt-5 mb-3">
        <div class="col col-2"></div>
        <div class="col col-8">
            <h4>STEP 2 - CONFIRM YOUR BOOKING DETAILS</h4>
            <hr>
        </div>
        <div class="col col-2"></div>
    </div>

    <?php
    foreach ($arrayTarifa as $tarifa) {
    ?>


    <div class="row">
        <div class="col col-2"></div>
        <div class="col col-8 shadow">
            <div class="row">
                <div class="col col-xl-5 col-lg-5 col-md-12 col-sm-12 col-12 mt-5 mb-5">
                    <img class="w-100 h-100" src="https://www.demediterraning.com/V05/img/dem/Alojamientos/72339/72339_64625019.jpg" alt="">
                </div>

                <div class="col col-xl-7 col-lg-7 col-md-12 col-sm-12 col-12 my-auto">
                    <div class="row mb-4">
                        <div class="col col-12 mt-3">
                            <h5>{{$tarifa->Nombre}}</h5>
                        </div>
                    </div>
                    <div class="row border-bottom m-1">
                        <div class="col col-6 text-left">
                            <p class="font-weight-bold mt-1">Check In</p>
                        </div>
                        <div class="col col-6 text-right mt-1">
                            <p>{{$arrayDatos["fechaInicio"]}}</p>
                        </div>
                    </div>



                    <div class="row border-bottom m-1">
                        <div class="col col-6 text-left">
                            <p class="font-weight-bold mt-1">Check Out</p>
                        </div>
                        <div class="col col-6 text-right mt-1">
                            <p>{{$arrayDatos["fechaFinal"]}}</p>
                        </div>
                    </div>




                    <div class="row border-bottom m-1">
                        <div class="col col-6 text-left">
                            <p class="font-weight-bold mt-1">Guest</p>
                        </div>
                        <div class="col col-6 text-right mt-1">
                            <p>{{$arrayDatos["adultos"]}}+{{$arrayDatos["niños"]}}</p>
                        </div>
                    </div>




                    <div class="row border-bottom m-1">
                        <div class="col col-6 text-left">
                            <p class="font-weight-bold mt-1">Number Of Night</p>
                        </div>
                        <div class="col col-6 text-right mt-1">
                            <p>{{$arrayDatos["noches"]}}</p>
                        </div>
                    </div>


                    <div class="row mt-4 mb-3">
                        <div class="col col-12 bg-secondary-light p-2">
                            <p class="text-danger text-right font-weight-bold">You Save 10%</p>
                            <h5 class="text-right font-weight-bold">Sub-Total: {{$arrayDatos["precio"]}}€</h5>
                        </div>
                    </div>




                </div>
            </div>
        </div>
        <div class="col col-2"></div>
    </div>


    <div class="row mt-5">
        <div class="col col-2"></div>
        <div class="col col-8 shadow p-3 mt-2">
            <!-- <p class="font-weight-bold">Total Price</p>

            <div class="row">
                <div class="col col-6">
                    <p>10% Services Charge</p>
                </div>
                <div class="col col-6 text-right">
                    <p>50.00 €</p>
                </div>
            </div>

            <div class="row text-danger font-weight-bold">
                <div class="col col-6">
                    <p>AMOUNT PAYABLE</p>
                </div>
                <div class="col col-6 text-right">
                    <p>200.00 € - 7% IGIG INCLUDED</p>
                </div>
            </div> -->

            <div class="row text-danger font-weight-bold mt-3">
                <div class="col col-4"></div>
                <div class="col col-4 text-center"><a href=<?php echo("/payment"."/".$arrayDatos["IdTarifa"]."/".$arrayDatos["fechaInicio"]."/".$arrayDatos["fechaFinal"]."/".$arrayDatos["adultos"]."/".$arrayDatos["niños"]."/".$arrayDatos["mascotas"]."/".$arrayDatos["noches"]."/".$arrayDatos["precio"]) ?> class="btn btn-block bg-light-only-blue text-white">NEXT</a></div>
                <div class="col col-4"></div>
            </div>

            <?php
            }
            ?>

        </div>
        <div class="col col-2"></div>
    </div>

    
@endsection