@extends('layouts.payment')

@section('content')

<a id="button"></a>



<div class="row mt-5">
    <div class="col col-3"></div>
    <div class="col col-6 text-center">
        <img class="" src="{{asset('./HomePage/img/logo.png')}}" alt="Logo La Piramide">
    </div>
    <div class="col col-3"></div>


</div>

<hr>


<div class="row mt-5">
    <div class="col col-2"></div>
    <div class="col col-8">
        <h4>STEP 3 - MAKE PAYMENT</h4>
    </div>
    <div class="col col-2"></div>
</div>

@if ($errors->any())

<div class="row justify-content-center">

    <div class="col-sm-7">

        <div class="alert alert-danger">

            <ul>

                @foreach($errors->all() as $error)

                <li>{{$error}}</li>

                @endforeach

            </ul>

        </div>

    </div>

</div>

@endif


<div class="row">
    <div class="col col-2">

    </div>
    <div class="col col-8">
        <div class="row ">
            <div class="col col-4">
                <img style="margin-right: -105px; width: 105%; height: 450px; margin-top: 100px;" src="https://cf.bstatic.com/images/hotel/max500/148/148427924.jpg" alt="">
            </div>
            <div class="col col-8">
                <form style="margin-left: -15px; height: 450px;" class="shadow w-100" id="regForm" method="POST" action=<?php echo ("/payment/createReserv" . "/" . $arrayDatos["IdTarifa"] . "/" . $arrayDatos["fechaInicio"] . "/" . $arrayDatos["fechaFinal"] . "/" . $arrayDatos["adultos"] . "/" . $arrayDatos["niños"] . "/" . $arrayDatos["mascotas"] . "/" . $arrayDatos["noches"] . "/" . $arrayDatos["precio"]) ?>>
                    <!-- One "tab" for each step in the form: -->
                    @csrf
                    <div class="tab">
                        <h5>Personal Information:</h5>
                        <div class="form-row mt-3">
                            <div class="col col-4">
                                <p><input class="rounded" id="nombre" placeholder="First name..." oninput="this.className = ''" name="nombre"></p>
                            </div>

                            <div class="col col-4">
                                <p><input class="rounded" id="apellidos" placeholder="Last name..." oninput="this.className = ''" name="apellidos"></p>
                            </div>

                            <div class="col col-4">
                                <p><input class="rounded" id="NIden" placeholder="DNI or PASSP" oninput="this.className = ''" name="N#Identificativo"></p>
                            </div>
                        </div>


                        <div class="form-row mt-3">
                            <div class="col col-6">
                                <p><input class="rounded" id="email" placeholder="Email..." oninput="this.className = ''" name="email"></p>
                            </div>

                            <div class="col col-6">
                                <p><input class="rounded" id="email2" placeholder="Confirm Email..." oninput="this.className = ''" name="email2"></p>
                            </div>
                        </div>

                        <div class="form-row mt-3">
                            <div class="col col-2">
                                <p><input class="rounded" id="prefijo" maxlength="4" placeholder="Prefix..." oninput="this.className = ''" name="prefTelefono" value="+"></p>
                            </div>

                            <div class="col col-5">
                                <p><input class="rounded" id="telefono" placeholder="Phone..." oninput="this.className = ''" name="telefono"></p>
                            </div>

                            <div class="col col-5">
                                <select class="rounded" name="pais" id="">
                                    <?php
                                    foreach ($arrayPaises as $paises) {
                                    ?>
                                        <option value=<?php echo ($paises->Id);  ?>>{{$paises->Nombre}}</option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>

                        </div>



                    </div>


                    <div class="tab">
                        <h5>Payment Method:</h5>

                        <div class="form-row mt-5">
                            <div class="col col-12 d-flex justify-content-around">

                                <img id="Reception" onclick="selectPayment('Reception');" style="width: 100px;" class="shadow rounded-circle" src="https://png.pngtree.com/png-vector/20190704/ourlarge/pngtree-reception-icon-in-trendy-style-isolated-background-png-image_1540312.jpg" alt="">
                                <img id="Paypal" onclick="selectPayment('Paypal');" style="width: 100px;" class="shadow rounded-circle" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRmBZTAlx4wXEjjOo0NVg4MAZB9FUqYjb0XmuGHwXa1avvpekspCSyY2vZVw1UVbOauAio&usqp=CAU" alt="">

                            </div>


                        </div>

                        <div id="form_Reception" class="form-row mt-5 mb-5">
                            <div class="col col-12">
                                <p class="text-center">The price of the reservation will be paid on the day of arrival</p>
                            </div>
                        </div>


                        <div id="form_Paypal" class="form-row mt-5 mb-5">
                            <div class="col col-12">
                                <p class="text-center">Currently this function is not operational</p>
                            </div>
                        </div>

                    </div>


                    <div class="tab text-center">
                        <h5 class="mt-5">Before continuing, check the data entered</h5>
                        <img class="mt-3" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS7N1rm_VYO3cd3wH2Sa3y56ylZDNmzVQVzChrvhG5SoNzJBAxqcBtTcjqAsAQj3R-xPo0&usqp=CAU" alt="">
                    </div>




                    <div class="" style="position: absolute; bottom: 150px; left: 60%; width: 350px;">

                        <div class="float-left">
                            <button type="button" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
                            <button type="button" id="nextBtn" onclick="nextPrev(1)">Next</button>
                        </div>

                    </div>
                    <!-- Circles which indicates the steps of the form: -->
                    <div style="position: absolute; bottom: 150px; left: 40%;">
                        <span class="step"></span>
                        <span class="step"></span>
                        <span class="step"></span>
                    </div>
                </form>
            </div>
        </div>



    </div>
    <div class="col col-2"></div>
</div>




<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>



<script>
    var btn = $('#button');

    $(window).scroll(function() {
        if ($(window).scrollTop() > 300) {
            btn.addClass('show');
        } else {
            btn.removeClass('show');
        }
    });

    btn.on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: 0
        }, '300');
    });
</script>

<script>
    var variableControl = false;
    var currentTab = 0; // Current tab is set to be the first tab (0)
    showTab(currentTab); // Display the current tab

    function showTab(n) {
        // This function will display the specified tab of the form...
        var x = document.getElementsByClassName("tab");
        x[n].style.display = "block";
        //... and fix the Previous/Next buttons:
        if (n == 0) {
            document.getElementById("prevBtn").style.display = "none";
        } else {
            document.getElementById("prevBtn").style.display = "inline";
        }
        if (n == (x.length - 1)) {
            document.getElementById("nextBtn").innerHTML = "Submit";
        } else {
            document.getElementById("nextBtn").innerHTML = "Next";
        }
        //... and run a function that will display the correct step indicator:
        fixStepIndicator(n)
    }

    function nextPrev(n) {


        var email = document.getElementById("email").value;
        var email2 = document.getElementById("email2").value;

        if(email != email2 ){
            alert("The emails must be the same");
            return;
        }

        // This function will figure out which tab to display
        var x = document.getElementsByClassName("tab");
        // Exit the function if any field in the current tab is invalid:
        if (n == 1 && !validateForm()) return false;
        // Hide the current tab:
        x[currentTab].style.display = "none";
        // Increase or decrease the current tab by 1:
        currentTab = currentTab + n;
        // if you have reached the end of the form...
        if (currentTab >= x.length) {
            // ... the form gets submitted:
            document.getElementById("regForm").submit();
            return false;
        }
        // Otherwise, display the correct tab:
        showTab(currentTab);
        

        if(variableControl == false){
            document.getElementById("nextBtn").style.display = "none";
            variableControl = true;
        }else if (currentTab == 0){
            document.getElementById("nextBtn").style.display = "inline";
        }else if (currentTab == 1 && document.getElementById("nextBtn").style.display == "inline"){
            document.getElementById("nextBtn").style.display = "none";
        }

    }

    function validateForm() {
        // This function deals with validation of the form fields
        var x, y, i, valid = true;
        x = document.getElementsByClassName("tab");
        y = x[currentTab].getElementsByTagName("input");
        // A loop that checks every input field in the current tab:
        for (i = 0; i < y.length; i++) {
            // If a field is empty...
            if (y[i].value == "") {
                // add an "invalid" class to the field:
                y[i].className += " invalid";
                // and set the current valid status to false
                valid = false;
            }
        }
        // If the valid status is true, mark the step as finished and valid:
        if (valid) {
            document.getElementsByClassName("step")[currentTab].className += " finish";
        }


        return valid; // return the valid status
    }

    function validarEmails(){


        

    }

    function fixStepIndicator(n) {
        // This function removes the "active" class of all steps...
        var i, x = document.getElementsByClassName("step");
        for (i = 0; i < x.length; i++) {
            x[i].className = x[i].className.replace(" active", "");
        }
        //... and adds the "active" class on the current step:
        x[n].className += " active";
    }
</script>

<script>
    document.getElementById("form_Reception").style.display = "none";
    document.getElementById("form_Card").style.display = "none";
    document.getElementById("form_Paypal").style.display = "none";




    function selectPayment(method) {

        document.getElementById("Reception").classList.remove("border", "border-primary");
        document.getElementById("Paypal").classList.remove("border", "border-primary");

        var methodSelect = document.getElementById(method);
        methodSelect.classList.add("border");
        methodSelect.classList.add("border-primary");


        switch (method) {
            case "Reception":

                document.getElementById("form_Paypal").style.display = "none";
                document.getElementById("form_Reception").style.display = "block";
                document.getElementById("nextBtn").style.display = "inline";

                break;

            case "Paypal":

                document.getElementById("form_Reception").style.display = "none";
                document.getElementById("form_Paypal").style.display = "block";
                document.getElementById("nextBtn").style.display = "none";
                break;


        }
    }
</script>

@endsection